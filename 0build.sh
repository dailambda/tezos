#!/bin/sh
set -e
if [ ! -d _opam ]; then
    opam switch create --no-install . ocaml-base-compiler.4.14.0
fi
eval $(opam env)
opam pin add -n plebeia.2.3.0 git+https://gitlab.com/dailambda/plebeia#8a3a19f27e077c06b42295aa1495360471d54841
opam pin add -n proc-smaps.0.2 git+https://gitlab.com/dailambda/proc-smaps#436d0ce3153e14f684b536cec3aa69d478b2de33
opam pin add -n blake3.0.3 git+https://github.com/camlspotter/ocaml-blake3#0.3
opam repo remove default
make build-deps
make
