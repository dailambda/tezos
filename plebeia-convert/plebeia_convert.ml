open Tezos_context
module X = Xcontext
module P = Pcontext

let doit () =
  let open Lwt_syntax in
  let dir = Sys.argv.(1) in
  let ch = Tezos_crypto.Context_hash.of_b58check_exn Sys.argv.(2) in
  let* index = X.init dir in
  let+ ctxtopt = X.checkout index ch in
  let ctxt = Stdlib.Option.get ctxtopt in
  match X.underlying_contexts ctxt with
  | _, Some pctxt ->
      let cur = P.cursor pctxt in
      let _, h = Plebeia.Cursor.compute_hash cur in
      let ch = P.to_context_hash (fst h) in
      Format.eprintf "Plebeia context hash: %a@." Tezos_crypto.Context_hash.pp ch
  | _ -> assert false

let () = Lwt_main.run @@ doit ()
