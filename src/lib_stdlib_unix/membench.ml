module Rusage = struct
  type t = {
    utime : float;
    stime : float;
    maxrss : int64;
    ixrss : int64;
    idrss : int64;
    isrss : int64;
    minflt : int64;
    majflt : int64;
    nswap : int64;
    inblock : int64;
    oublock : int64;
    msgsnd : int64;
    msgrcv : int64;
    nsignals : int64;
    nvcsw : int64;
    nivcsw : int64;
  }

  type who = SELF | CHILDREN

  external get : who -> t = "unix_getrusage"

  let get_maxrss () =
    let n =
      let r = get SELF in
      r.maxrss
    in
    let open Sys_info in
    match uname with Ok Linux -> Int64.mul n 1024L | Ok Darwin -> n | _ -> n
end

module Gc = Gc

let du d =
  let open Lwt in
  Lwt_process.(pread_line ("du", [|"du"; "-s"; "-k"; d|])) >|= fun s ->
  try
    let n = String.index s '\t' in
    Some (Int64.of_string @@ String.sub s 0 n)
  with _ -> None

let getenv s =
  match Sys.getenv s with exception Not_found -> None | s -> Some s

let existenv s =
  match Sys.getenv s with exception Not_found -> false | _ -> true

module Sys = Sys

let gc_stat =
  match getenv "BENCH_GC_REPORT" with
  | None -> None
  | Some x -> (
      match int_of_string x with
      | n when n > 0 -> Some n
      | _ | (exception _) -> Some 1)

let get_rss () =
  match Sys_info.memory_stats_no_lwt () with
  | Error _ -> -1L
  | Ok (Statm {resident; _}) -> resident
  | Ok (Ps {resident; _}) -> resident

let i = ref 0

let report_memory ppf =
  let pid = Unix.getpid () in
  let stat = Gc.quick_stat () in
  Format.fprintf
    ppf
    "pid: %d maxrss: %.02f GiB rss: %.02f GiB major: %d compaction: %d"
    pid
    (Int64.to_float (Rusage.get_maxrss ()) /. 1024. /. 1024. /. 1024.)
    (Int64.to_float (get_rss ()) *. 4096. /. 1024. /. 1024. /. 1024.)
    stat.major_collections
    stat.compactions ;
  incr i ;
  match gc_stat with
  | None -> ()
  | Some n ->
      if !i mod n = 0 then
        let t1 = Mtime_clock.elapsed_ns () in
        let stat = Gc.stat () in
        let t2 = Mtime_clock.elapsed_ns () in
        Format.fprintf
          ppf
          " live_words: %.02fGB in %.02fsecs"
          (Stdlib.float stat.live_words
          *. Stdlib.float Sys.word_size /. 8. /. 1024. /. 1024. /. 1024.)
          (Int64.(to_float (sub t2 t1)) /. 1_000_000_000.0)
