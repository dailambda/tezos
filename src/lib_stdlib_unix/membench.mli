module Rusage : sig
  type t = {
    utime : float;
    stime : float;
    maxrss : int64;
    ixrss : int64;
    idrss : int64;
    isrss : int64;
    minflt : int64;
    majflt : int64;
    nswap : int64;
    inblock : int64;
    oublock : int64;
    msgsnd : int64;
    msgrcv : int64;
    nsignals : int64;
    nvcsw : int64;
    nivcsw : int64;
  }

  type who = SELF | CHILDREN

  val get : who -> t

  (** Returns maxrss in *bytes* in Linux and Darwin.
      In other unames, returns the original maxrss value of getrusage().
  *)
  val get_maxrss : unit -> int64
end

module Gc : sig
  type stat = {
    minor_words : float;
        (** Number of words allocated in the minor heap since
         the program was started. *)
    promoted_words : float;
        (** Number of words allocated in the minor heap that
         survived a minor collection and were moved to the major heap
         since the program was started. *)
    major_words : float;
        (** Number of words allocated in the major heap, including
         the promoted words, since the program was started. *)
    minor_collections : int;
        (** Number of minor collections since the program was started. *)
    major_collections : int;
        (** Number of major collection cycles completed since the program
          was started. *)
    heap_words : int;  (** Total size of the major heap, in words. *)
    heap_chunks : int;
        (** Number of contiguous pieces of memory that make up the major heap. *)
    live_words : int;
        (** Number of words of live data in the major heap, including the header
         words. *)
    live_blocks : int;  (** Number of live blocks in the major heap. *)
    free_words : int;  (** Number of words in the free list. *)
    free_blocks : int;  (** Number of blocks in the free list. *)
    largest_free : int;
        (** Size (in words) of the largest block in the free list. *)
    fragments : int;
        (** Number of wasted words due to fragmentation.  These are
         1-words free blocks placed between two live blocks.  They
         are not available for allocation. *)
    compactions : int;
        (** Number of heap compactions since the program was started. *)
    top_heap_words : int;
        (** Maximum size reached by the major heap, in words. *)
    stack_size : int;  (** Current size of the stack, in words. @since 3.12.0 *)
    forced_major_collections : int;
        (** Number of forced full major collections completed since the program
          was started. @since 4.12.0 *)
  }

  (** Return the current values of the memory management counters in a
     [stat] record.  This function examines every heap block to get the
     statistics. *)
  val stat : unit -> stat

  (** Same as [stat] except that [live_words], [live_blocks], [free_words],
      [free_blocks], [largest_free], and [fragments] are set to 0.  This
      function is much faster than [stat] because it does not need to go
      through the heap. *)
  val quick_stat : unit -> stat

  (** Return [(minor_words, promoted_words, major_words)].  This function
      is as fast as [quick_stat]. *)
  val counters : unit -> float * float * float

  (** Number of words allocated in the minor heap since the program was
      started. This number is accurate in byte-code programs, but only an
      approximation in programs compiled to native code.

      In native code this function does not allocate.

      @since 4.04 *)
  val minor_words : unit -> float

  (** Trigger a minor collection. *)
  val minor : unit -> unit

  (** [major_slice n]
      Do a minor collection and a slice of major collection. [n] is the
      size of the slice: the GC will do enough work to free (on average)
      [n] words of memory. If [n] = 0, the GC will try to do enough work
      to ensure that the next automatic slice has no work to do.
      This function returns an unspecified integer (currently: 0). *)
  val major_slice : int -> int

  (** Do a minor collection and finish the current major collection cycle. *)
  val major : unit -> unit

  (** Do a minor collection, finish the current major collection cycle,
     and perform a complete new cycle.  This will collect all currently
     unreachable blocks. *)
  val full_major : unit -> unit

  (** Perform a full major collection and compact the heap.  Note that heap
     compaction is a lengthy operation. *)
  val compact : unit -> unit

  (** Print the current values of the memory management counters (in
     human-readable form) into the channel argument. *)
  val print_stat : out_channel -> unit

  (** Return the total number of bytes allocated since the program was
     started.  It is returned as a [float] to avoid overflow problems
     with [int] on 32-bit machines. *)
  val allocated_bytes : unit -> float

  (** [Memprof] is a sampling engine for allocated memory words. Every
     allocated word has a probability of being sampled equal to a
     configurable sampling rate. Once a block is sampled, it becomes
     tracked. A tracked block triggers a user-defined callback as soon
     as it is allocated, promoted or deallocated.

     Since blocks are composed of several words, a block can potentially
     be sampled several times. If a block is sampled several times, then
     each of the callback is called once for each event of this block:
     the multiplicity is given in the [n_samples] field of the
     [allocation] structure.

     This engine makes it possible to implement a low-overhead memory
     profiler as an OCaml library.

     Note: this API is EXPERIMENTAL. It may change without prior
     notice. *)
  module Memprof : sig
    type allocation_source = Normal | Marshal | Custom

    (** The type of metadata associated with allocations. This is the
         type of records passed to the callback triggered by the
         sampling of an allocation. *)
    type allocation = private {
      n_samples : int;  (** The number of samples in this block (>= 1). *)
      size : int;  (** The size of the block, in words, excluding the header. *)
      source : allocation_source;  (** The type of the allocation. *)
      callstack : Printexc.raw_backtrace;
          (** The callstack for the allocation. *)
    }

    (**
         A [('minor, 'major) tracker] describes how memprof should track
         sampled blocks over their lifetime, keeping a user-defined piece
         of metadata for each of them: ['minor] is the type of metadata
         to keep for minor blocks, and ['major] the type of metadata
         for major blocks.

         When using threads, it is guaranteed that allocation callbacks are
         always run in the thread where the allocation takes place.

         If an allocation-tracking or promotion-tracking function returns [None],
         memprof stops tracking the corresponding value.
       *)
    type ('minor, 'major) tracker = {
      alloc_minor : allocation -> 'minor option;
      alloc_major : allocation -> 'major option;
      promote : 'minor -> 'major option;
      dealloc_minor : 'minor -> unit;
      dealloc_major : 'major -> unit;
    }

    (** Default callbacks simply return [None] or [()] *)
    val null_tracker : ('minor, 'major) tracker

    (** Start the sampling with the given parameters. Fails if
         sampling is already active.

         The parameter [sampling_rate] is the sampling rate in samples
         per word (including headers). Usually, with cheap callbacks, a
         rate of 1e-4 has no visible effect on performance, and 1e-3
         causes the program to run a few percent slower

         The parameter [callstack_size] is the length of the callstack
         recorded at every sample. Its default is [max_int].

         The parameter [tracker] determines how to track sampled blocks
         over their lifetime in the minor and major heap.

         Sampling is temporarily disabled when calling a callback
         for the current thread. So they do not need to be re-entrant if
         the program is single-threaded. However, if threads are used,
         it is possible that a context switch occurs during a callback,
         in this case the callback functions must be re-entrant.

         Note that the callback can be postponed slightly after the
         actual event. The callstack passed to the callback is always
         accurate, but the program state may have evolved. *)
    val start :
      sampling_rate:float ->
      ?callstack_size:int ->
      ('minor, 'major) tracker ->
      unit

    (** Stop the sampling. Fails if sampling is not active.

          This function does not allocate memory.

          All the already tracked blocks are discarded. If there are
          pending postponed callbacks, they may be discarded.

          Calling [stop] when a callback is running can lead to
          callbacks not being called even though some events happened. *)
    val stop : unit -> unit
  end
end

(** Return the size of the file or directory in KB by running [du -s -k <dir>] *)
val du : string -> int64 option Lwt.t

val getenv : string -> string option

val existenv : string -> bool

val get_rss : unit -> int64

val report_memory : Format.formatter -> unit
