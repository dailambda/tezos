(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Tezos - Versioned (key x value) store over Plebeia *)

open Plebeia.Utils
module P = Plebeia
module F = Plebeia.Fs_tree
module Vc = F.Vc

module IContext = Context
open Legacy_monad_globals

let nameenc =
  match Sys.getenv "PCONTEXT_NAMEENC" with
  | exception Not_found -> `Compressed
  | "bits8" -> `Bits8
  | "compressed" -> `Compressed
  | _ -> Stdlib.failwith "PCONTEXT_NAMEENC=bits8|compressed"

(*-- Initialization and logging ----------------------------------*)

let () =
  (* Override the hash printer of Plebeia *)
  let hash_show h =
    Tezos_crypto.Context_hash.to_b58check @@ P.Result.from_Ok @@ Tezos_crypto.Context_hash.of_string
    @@
    let s = P.Hash.Prefix.to_string h in
    s ^ String.make (32 - String.length s) '\000'
  in
  P.Hash.Prefix.show_ref := hash_show ;
  (* Override the logging of Plebeia *)
  let module PLog = Internal_event.Legacy_logging.Make (struct
    let name = "plebeia"
  end) in
  let open P.Log in
  let f level fmt =
    let f =
      match level with
      | Debug -> PLog.debug
      | Info -> PLog.log_info
      | Notice -> PLog.log_notice
      | Warning -> PLog.warn
      | Error -> PLog.log_error
      | Fatal -> PLog.fatal_error
    in
    f fmt
  in
  P.Log.(set {f})

(* Note: Log.debug messages are never flushed if there is no real Lwt context switches
   and cause memory leak.
*)
module Log = Internal_event.Legacy_logging.Make (struct
  let name = "pcontext"
end)

(*-- Error ----------------------------------*)

type error += Plebeia_error of string

(* Convert Plebeia errors to Tezos errors *)
let wrap_plebeia_error = function
  | Ok v -> Ok v
  | Error e -> Error [Plebeia_error (Format.asprintf "%a" P.Error.pp e)]

let () =
  let open Data_encoding in
  register_error_kind
    `Permanent
    ~id:"Plebeia_error"
    ~title:"Plebeia error"
    ~description:"Error from Plebeia"
    ~pp:(fun ppf s -> Format.fprintf ppf "Plebeia error: %s" s)
    (obj1 (req "desc" string))
    (function Plebeia_error s -> Some s | _ -> None)
    (fun s -> Plebeia_error s)

let lwt_failf fmt = Format.kasprintf (fun s -> Lwt.fail (Failure s)) fmt

let lwt_fail_at_plebeia_error = function
  | Ok x -> Lwt.return x
  | Error e -> lwt_failf "Plebeia: error: %a@." Plebeia.Error.pp e

let fail_at_plebeia_error = function
  | Ok x -> x
  | Error e -> failwithf "Plebeia: error: %a@." Plebeia.Error.pp e

(* Abort the node main thread with the stack trace *)
let force_exit fmt =
  let stack = Printexc.(raw_backtrace_to_string (get_callstack 10)) in
  Format.kasprintf
    (fun s ->
      Lwt.async (fun () ->
          Log.lwt_fatal_error "%s" s >>= fun () ->
          Log.lwt_fatal_error "%s" stack >>= fun () ->
          Log.lwt_fatal_error "force exitting..." >>= fun () ->
          Lwt_exit.exit_and_raise 1) ;
      assert false)
    fmt

(*-- Debug -----------------------------------------------*)

module Debug = struct
  (* raw_set is read back for testing if the env var is set *)
  let checkenv n = try Sys.getenv n <> "" with _ -> false
end

(*-- Key --------------------------------------------------*)

(* type key = string list *)

let string_of_key key =
  "/" ^ String.concat "/"
  @@ List.map (fun k -> if k = "" then "<empty>" else k) key

let pp_key ppf key = Format.string ppf @@ string_of_key key

let current_protocol_key = ["protocol"]

let current_test_chain_key = ["test_chain"]

let current_data_key = "data"

let current_predecessor_block_metadata_hash_key =
  ["predecessor_block_metadata_hash"]

let current_predecessor_ops_metadata_hash_key =
  ["predecessor_ops_metadata_hash"]

let data_key key = current_data_key :: key

(*-- Value ------------------------------------------------*)

(* type value = bytes *)

(*-- Hashes -----------------------------------------------*)

let to_context_hash hp =
  Tezos_crypto.Context_hash.of_string_exn @@ P.Hash.Prefix.to_string hp ^ "\000\000\000\000"

let to_plebeia_commit_hash ch =
  P.Commit_hash.of_string @@ Tezos_crypto.Context_hash.to_string ch

let of_plebeia_commit_hash ch =
  Tezos_crypto.Context_hash.of_string @@ P.Commit_hash.to_string ch

module Make (Encoding : module type of Tezos_context_encoding.Context) = struct
  module I = IContext.Make (Encoding)

  type error +=
    | Cannot_create_file = I.Cannot_create_file
    | Cannot_open_file = I.Cannot_open_file
    | Cannot_retrieve_commit_info of Tezos_crypto.Context_hash.t
    | Cannot_find_protocol = I.Cannot_find_protocol
    | Suspicious_file = I.Suspicious_file

  (*-- Types ------------------------------------------------*)

  type tree = F.tree ref

  type index = {
    path : string;  (** root dir *)
    patch_context : (context -> context tzresult Lwt.t) option;
    pcontext : Vc.t;
  }

  and context = {
    index : index;
    parent : P.Commit_hash.t option;
    based_on : Tezos_crypto.Context_hash.t option;
    based_cur : F.tree;
    tree : tree;
    (* If [true], the parent may not be found in Plebeia context. *)
    may_miss_parent : bool;
  }

  type t = context

  let index t = t.index

  let vc i = i.pcontext

  let _pp_context ppf ctxt =
    match ctxt.based_on with
    | None -> Format.fprintf ppf "none"
    | Some ch -> Tezos_crypto.Context_hash.pp ppf ch

  (*-- Cursor cache ----------------------------------------*)

  module CursorCache : sig
    (* Cursor cache by Plebeia Index.t

       As of 2020-04-15, Tezos checks out one context hash 4 times consecutively.
       We can reuse the cursor of the commit as far as the tree is never modified
       in order to avoid loading same nodes repeatedly.
    *)

    (** Cursor should be re-registered for each update to maximize the efficiency.
        If the cursor is not indexed, i.e. modified, this function never register it.
    *)
    val may_register : F.tree -> unit

    val get : P.Index.t -> F.tree option
  end = struct
    let enabled =
      match Sys.getenv "PCONTEXT_DISABLE_CURSOR_CACHE" with
      | _ ->
          (* XXX This happens before the Log initialization and discarded *)
          Log.fatal_error "Configuration: no cursor cache" ;
          false
      | exception _ -> true

    (* The cursor cache needs not be huge.  We simply keep the last 10 cursors.
    *)
    module Tbl = Hashtbl.Make (struct
      type t = P.Index.t

      let hash = Hashtbl.hash

      let equal = ( = )
    end)

    let queue, cache = (Queue.create (), Tbl.create 101)

    let size = 10

    let may_register cur =
      if not enabled then ()
      else
        match F.index cur with
        | None -> () (* modified, therefore never cached *)
        | Some i ->
            if Tbl.mem cache i then (
              Tbl.replace cache i cur ;
              assert (Tbl.length cache <= size))
            else (
              Tbl.replace cache i cur ;
              Queue.add i queue ;
              (* Forget old *)
              while Queue.length queue > size do
                let i_old = Queue.take queue in
                Tbl.remove cache i_old
              done)

    let get = Tbl.find cache
  end

  (*-- Initialization ----------------------------------------------------------*)

  (* root_context = "/home/foobar/.tezos-node/context" for ex. *)
  let init ?patch_context ?readonly root_context =
    let root = Filename.dirname root_context in
    (* 2 files: "/home/foobar/.tezos-node/plebeia.context"
                "/home/foobar/.tezos-node/plebeia.roots" *)
    let fn = Filename.concat root "plebeia" in
    let node_cache =
      if Debug.checkenv "PCONTEXT_ENABLE_NODE_CACHE" then (
        Log.fatal_error "Configuration: enable node cache" ;
        Some P.Node_cache.(create ()))
      else None
    in
    (* XXX Plebeia.Option should inherit Stdlib.Option *)
    let readonly = Stdlib.Option.value readonly ~default:false in
    let nameenc =
      match nameenc with
      | `Compressed -> P.Fs_nameenc.compressed (fn ^ ".names.txt")
      | `Bits8 -> P.Fs_nameenc.bits8
    in
    (match readonly with
    | true ->
        Lwt.catch (* XXX *)
          (fun () -> Vc.open_existing_for_read P.Context.default_config nameenc fn)
          (fun _ ->
            (* if the file does not exist, create an empty one *)
            Log.fatal_error "Create an empty Plebeia context file" ;
            Vc.open_for_write
              ?node_cache
              P.Context.default_config
              nameenc
              fn
            >>=? fun vc ->
            Vc.close vc >>=? fun () ->
            Vc.open_existing_for_read P.Context.default_config nameenc fn)
    | _ ->
        Vc.open_for_write
          ?node_cache
          P.Context.default_config
          nameenc
          fn)
    >>= function
    | Ok pcontext -> Lwt.return {path = root; patch_context; pcontext}
    | Error e ->
        Log.fatal_error "Error: %a@." P.Error.pp e;
        assert false

  let close index = Vc.close index.pcontext >>= lwt_fail_at_plebeia_error

  (*-- Context FS APIs ---------------------------------------------*)

  module Tree = struct
    let pp ppf _c = Format.fprintf ppf "<tree>"

    (* rw *)
    let run_rw cref (m : 'a F.Op.t) =
      let c = !cref in
      m c >|? fun (c', v) -> (ref c', v)
    (* c' may be different from c *)

    (* ro *)
    let run_ro cref (m : 'a F.Op.t) =
      let c = !cref in
      m c >|? fun (c', v) ->
      cref := c' ;
      (* c' is equal to c, with some nodes loaded *)
      v

    (* ro, lwt version *)
    let run_ro_lwt cref (m : 'a F.Op_lwt.t) =
      let c = !cref in
      m c >|=? fun (c', v) ->
      cref := c' ;
      v

    (** {2 Getters} *)

    let raw_get_cache = ref None

    let raw_get tree key =
      match !raw_get_cache with
      | Some (tree', key', res) when tree == tree' && key = key' -> res
      | _ ->
          let res = run_ro tree @@ F.Op.get_tree key in
          raw_get_cache := Some (tree, key, res) ;
          res

    let mem tree key =
      Lwt.return
      @@
      match raw_get tree key with
      | Error _ -> false
      | Ok (_, P.Node_type.Leaf _) -> true
      | Ok (_, Bud _) -> false
      | _ -> assert false

    let mem_tree tree key =
      Lwt.return
      @@
      match raw_get tree key with
      | Error _ -> false
      | Ok (_, Leaf _) -> true
      | Ok (_, Bud _) -> true
      | _ -> assert false

    let find tree key =
      Lwt.return
      @@
      match raw_get tree key with
      | Ok (c, (Leaf _ as leaf)) ->
          let ctxt = F.context c in
          let v = P.Node_storage.read_leaf_value ctxt leaf in
          Some (P.Value.to_bytes v)
      | Ok (_c, Bud _) -> None
      | Error _ -> None
      | _ -> assert false

    let find_tree tree key =
      Lwt.return
      @@
      match raw_get tree key with Ok (c, _) -> Some (ref c) | Error _ -> None

    let list tree dir =
      run_ro_lwt tree (F.Op_lwt.ls dir) >|= function
      | Error (P.Fs_types.FsError.FS_error (No_such_file_or_directory _)) ->
          (* Plebeia's fold fails if the target does not exist.
             In Tezos, it is not an error. *)
          []
      | Error e ->
          failwithf "Pcontext.Tree.list: %a : %s" pp_key dir (P.Error.show e)
      | Ok ks ->
          let ks = List.map (fun (n, c) -> (n, ref c)) ks in
          List.sort (fun (n1, _) (n2, _) -> compare n1 n2) ks

    let list tree ?offset ?length dir =
      match (offset, length) with
      | None, None -> list tree dir
      | _ -> (
          let offset = P.Option.default 0 offset in
          let length = P.Option.default max_int length in
          run_ro_lwt tree (F.Op_lwt.ls2 ~offset ~length dir) >|= function
          | Error (P.Fs_types.FsError.FS_error (No_such_file_or_directory _)) ->
              (* Plebeia's fold fails if the target does not exist.
                 In Tezos, it is not an error. *)
              []
          | Error e ->
              failwithf
                "Pcontext.Tree.list: %a : %s"
                pp_key
                dir
                (P.Error.show e)
          | Ok ks ->
              let ks = List.map (fun (n, c) -> (n, ref c)) ks in
              List.sort (fun (n1, _) (n2, _) -> compare n1 n2) ks)

    let length tree key =
      let open Lwt.Syntax in
      let+ res = run_ro_lwt tree (F.Op_lwt.count key) in
      match res with
      | Error _ -> assert false
      | Ok n -> n

    (** {2 Setters} *)

    let add_tree tree key tree' =
      Lwt.return
      @@
      match run_rw tree @@ F.Op.set_tree key !tree' with
      | Ok (tree', ()) -> tree'
      | Error e ->
          Log.fatal_error
            "Pcontext.Tree.add_tree: %a : %s"
            pp_key
            key
            (P.Error.show e) ;
          failwithf
            "Pcontext.Tree.add_tree: %a : %s"
            pp_key
            key
            (P.Error.show e)

    let add tree key v =
      Lwt.return
      @@
      match run_rw tree @@ F.Op.write key (P.Value.of_bytes v) with
      | Ok (tree', ()) -> tree'
      | Error e ->
          Log.fatal_error
            "Pcontext.Tree.add: %a : %s"
            pp_key
            key
            (P.Error.show e) ;
          failwithf "Pcontext.Tree.add: %a : %s" pp_key key (P.Error.show e)

    let remove tree key =
      Lwt.return
      @@
      match run_rw tree @@ F.Op.rm ~recursive:true ~ignore_error:true key with
      | Error e ->
          Log.log_error
            "Pcontext.Tree.remove: %a : %s"
            pp_key
            key
            (P.Error.show e) ;
          tree
      | Ok (tree, _) -> tree

    (** {2 Fold} *)

    (* type depth = [`Eq of int | `Le of int | `Lt of int | `Ge of int | `Gt of int] *)

    let raw_fold ?depth tree key ~init ~f =
      run_ro_lwt tree
      @@ F.Op_lwt.fold' ?depth init key (fun acc key tree ->
             f key (ref tree) acc >|= ok)
      >|= function
      | Ok res -> res
      | Error (P.Fs_types.FsError.FS_error (No_such_file_or_directory _)) ->
          (* Plebeia's fold fails if the target does not exist.
             In Tezos, it is not an error. *)
          init
      | Error e -> failwithf "Pcontext.Tree.fold: %s" (P.Error.show e)

    let fold ?depth tree key ~order ~init ~f =
      match order with
      | `Undefined -> raw_fold ?depth tree key ~init ~f
      | `Sorted ->
          raw_fold ?depth tree key ~init:[] ~f:(fun k tref acc ->
              Lwt.return ((k,tref)::acc)) >>= fun xs ->
          let xs = List.sort (fun (k1,_) (k2,_) -> compare k1 k2) xs in
          Lwt_list.fold_left_s (fun acc (k,tref) ->
              f k tref acc) init xs

    (** {2 Tree} *)

    let empty ctxt : tree =
      let cur = F.empty (Vc.context ctxt.index.pcontext) ctxt.index.pcontext.enc in
      ref cur

    let view cref =
      let c, v =
        match F.Op.get_tree [] !cref with
        | Ok (c, (_, v)) -> (c, v)
        | Error _ -> assert false
      in
      cref := c ;
      v

    let is_empty (cref : tree) =
      let v = view cref in
      match v with
      | Bud (None, _, _) -> true
      | Bud _ | Leaf _ -> false
      | _ -> assert false

    let kind (cref : tree) =
      let v = view cref in
      match v with Bud _ -> `Tree | Leaf _ -> `Value | _ -> assert false

    let to_value (cref : tree) =
      Lwt.return
      @@
      let v = view cref in
      match v with
      | Bud _ -> None
      | Leaf _ ->
          let ctxt = F.context !cref in
          let v = P.Node_storage.read_leaf_value ctxt v in
          Some (P.Value.to_bytes v)
      | _ -> assert false

    let of_value ctxt v =
      let tree = Vc.of_value ctxt.index.pcontext @@ P.Value.of_bytes v in
      let cref : tree = ref tree in
      Lwt.return cref

    let hash (tree : tree) =
      let cur, h =
        match F.Op.compute_hash [] !tree with
        | Ok (cur, h) -> (cur, h)
        | Error _ -> assert false
      in
      tree := cur ;
      to_context_hash h

    let equal tree1 tree2 =
      let h1 = hash tree1 in
      let h2 = hash tree2 in
      h1 = h2

    let clear ?depth (t : tree) =
      assert (depth = None) ;
      (* XXX not supported *)
      match F.Op.may_forget [] !t with Error _ -> () | Ok (c, ()) -> t := c

    (** {2 Raw (tree of bytes)} *)

    type raw =
      [`Value of bytes | `Tree of raw Tezos_base.TzPervasives.String.Map.t]

    let raw_encoding = I.Tree.raw_encoding

    let to_raw _ = assert false

    let of_raw _ = assert false

    type repo = unit

    let make_repo = Lwt.return

    (** {2 Merkle proof} *)

    let merkle_proof tree keys =
      run_ro
        tree
        (let open F.Op.Syntax in
        let+ proof, _details = F.Merkle_proof.make [] keys in
        proof)
      |> wrap_plebeia_error
  end

  let raw_find ctxt dkey = Tree.find ctxt.tree dkey

  let find ctxt key = raw_find ctxt @@ data_key key

  let find_tree ctxt key = Tree.find_tree ctxt.tree @@ data_key key

  let mem ctxt key = Tree.mem ctxt.tree @@ data_key key

  let mem_tree ctxt key = Tree.mem_tree ctxt.tree @@ data_key key

  let fold ?depth ctxt key ~order ~init ~f =
    Tree.fold ?depth ctxt.tree (data_key key) ~order ~init ~f

  let raw_fold ?depth ctxt key ~init ~f ~order =
    Tree.fold ?depth ctxt.tree key ~order ~init ~f

  let length ctxt key = Tree.length ctxt.tree key

  let list ctxt ?offset ?length dir =
    Tree.list ctxt.tree ?offset ?length @@ data_key dir

  let merkle_proof ctxt keys =
    Tree.merkle_proof ctxt.tree (List.map (fun k -> data_key k) keys)

  let wrap_rw ctxt f = f ctxt.tree >|= fun tree -> {ctxt with tree}

  let raw_remove ctxt dkey =
    wrap_rw ctxt @@ fun tree -> Tree.remove tree @@ dkey

  let remove t key = raw_remove t @@ data_key key

  let raw_add ctxt dkey data =
    wrap_rw ctxt @@ fun tree -> Tree.add tree dkey data

  let add t key data = raw_add t (data_key key) data

  let add_tree ctxt key tree =
    wrap_rw ctxt @@ fun tree' -> Tree.add_tree tree' (data_key key) tree

  (* -- Stats ---------------------------------------- *)

  let get_size t = P.Storage.size (Vc.context t.pcontext).storage

  (*-- Commit access and update -----------------------------------------------*)

  let exists index key =
    let hash = to_plebeia_commit_hash key in
    (* We cannot use P.Commit.mem since it does not sync the states in the Readers *)
    Vc.mem index.pcontext hash

  let checkout index key =
    let hash = to_plebeia_commit_hash key in
    Log.lwt_debug "checkout... %a %a" Tezos_crypto.Context_hash.pp key P.Commit_hash.pp hash
    >>= fun () ->
    Vc.checkout index.pcontext hash >>= function
    | None -> Lwt.return_none
    | Some tree ->
        (* If the same commit was checked out recently, let's use the older
           one with nodes loaded into memory *)
        let tree, _found =
          match F.index tree with
          | None -> (tree (* impossible, but ok *), false)
          | Some i -> (
              match CursorCache.get i with
              | None ->
                  CursorCache.may_register tree ;
                  (tree, false)
              | Some tree -> (tree, true))
        in
        (* Since it contains copy information, we must reset it *)
        let tree = F.forget_info tree in
        Log.lwt_debug "checkout %a done." Tezos_crypto.Context_hash.pp key >>= fun () ->
        let tree = ref tree in
        (* Note that key and hash are the hashes of THIS commit, not the parent.
           They WILL be the parent of the next commit.
        *)
        let pc =
          {
            index;
            tree;
            based_cur = !tree;
            based_on = Some key;
            parent = Some hash;
            may_miss_parent = false;
          }
        in
        Lwt.return_some pc

  let checkout_exn index key =
    checkout index key >>= function
    | None -> Lwt.fail Not_found
    | Some p -> Lwt.return p

  let get_hash_version _c = Tezos_crypto.Context_hash.Version.of_int 0

  let set_hash_version c v =
    if Tezos_crypto.Context_hash.Version.(of_int 0 = v) then return c
    else fail (Tezos_context_helpers.Context.Unsupported_context_hash_version v)

  let commit ~hash_override ~time ?message context =
    (match hash_override with
    | None -> Lwt.return ()
    | Some hash_override ->
        Log.lwt_debug
          "committing: override %a ..."
          Tezos_crypto.Context_hash.pp
          hash_override)
    >>= fun () ->
    let time = Time.Protocol.to_seconds time in
    let message = match message with None -> "" | Some m -> m in
    raw_add context ["time"] (Bytes.of_string @@ Int64.to_string time)
    >>= fun context ->
    raw_add context ["message"] (Bytes.of_string message) >>= fun context ->
    let t1 = Time.System.now () in

    (let hash_override = Option.map to_plebeia_commit_hash hash_override in
     let tree = F.top_tree !(context.tree) in
     Vc.commit
       ~allow_missing_parent:context.may_miss_parent
       context.index.pcontext
       ~parent:context.parent
       ~hash_override
       tree
     >>= lwt_fail_at_plebeia_error
     >>= fun res ->
     (* XXX Currently we call msync for each commit *)
     Vc.flush context.index.pcontext >|= fun () -> res)
    >>= fun (tree_at_root, (phash, _commit)) ->
    let t2 = Time.System.now () in
    CursorCache.may_register tree_at_root ;
    let ch =
      (* the final context hash *)
      match hash_override with
      | Some ch -> ch
      | None -> to_context_hash phash
    in
    Log.lwt_log_notice
      "commit: %a in %a %.5f"
      Tezos_crypto.Context_hash.pp
      ch
      Ptime.Span.pp
      (Ptime.diff t2 t1)
      (Ptime.Span.to_float_s (Ptime.diff t2 t1))
    >>= fun () ->
    Log.lwt_log_notice
      "commit: plebeia hash: %a"
      P.Hash.Prefix.pp phash
    >>= fun () ->
    context.tree := tree_at_root ;
    Lwt.return (ch, phash)

  let hash ~time ?message context =
    let time = Time.Protocol.to_seconds time in
    let message = match message with None -> "" | Some m -> m in
    (* we cannot use raw_add since the function must be non lwt *)
    let fcur =
      match
        F.Op.run
          !(context.tree)
          (let open F.Op in
          let open F.Op.Syntax in
          let* () =
            write
              ["time"]
              (P.Value.of_bytes (Bytes.of_string @@ Int64.to_string time))
          in
          write ["message"] (P.Value.of_bytes (Bytes.of_string message)))
      with
      | Error e -> failwithf "Pcontext.hash: %s" (P.Error.show e)
      | Ok (fcur, ()) -> fcur
    in
    let tree_at_root = F.top_tree fcur in
    let _cur, phash = F.compute_hash tree_at_root in
    to_context_hash @@ fst phash

  (* -- Predefined Fields -------------------------------------------------------*)

  let get_protocol v =
    raw_find v current_protocol_key >>= function
    | None ->
        (* Irmin calls [assert false] *)
        force_exit "get_protocol: protocol not found"
    | Some data -> Lwt.return (Tezos_crypto.Protocol_hash.of_bytes_exn data)

  let add_protocol v key =
    raw_add v current_protocol_key @@ Tezos_crypto.Protocol_hash.to_bytes key

  let get_test_chain v =
    raw_find v current_test_chain_key >>= function
    | None -> lwt_failf "Unexpected error (Context.get_test_chain)"
    | Some data -> (
        match
          Data_encoding.Binary.of_bytes_opt Test_chain_status.encoding data
        with
        | None -> lwt_failf "Unexpected error (Context.get_test_chain)"
        | Some r -> Lwt.return r)

  let add_test_chain v id =
    raw_add v current_test_chain_key
    @@ Data_encoding.Binary.to_bytes_exn Test_chain_status.encoding id

  let remove_test_chain v = raw_remove v current_test_chain_key

  let fork_test_chain v ~protocol ~expiration =
    add_test_chain v (Forking {protocol; expiration})

  let get_branch chain_id = Format.asprintf "%a" Tezos_crypto.Chain_id.pp chain_id

  let empty index =
    let fcur = F.empty (Vc.context index.pcontext) index.pcontext.enc in
    {
      index;
      parent = None;
      tree = ref fcur;
      based_cur = fcur;
      based_on = None;
      may_miss_parent = false;
    }

  let commit_genesis ~hash_override index ~chain_id:_ ~time ~protocol =
    let ctxt = empty index in
    (match index.patch_context with
    | None -> Lwt.return (Ok ctxt)
    | Some patch_context -> patch_context ctxt)
    >>=? fun ctxt ->
    add_protocol ctxt protocol >>= fun ctxt ->
    add_test_chain ctxt Not_running >>= fun ctxt ->
    commit ~time ~message:"Genesis" ctxt ~hash_override >>= return

  let compute_testchain_chain_id =
    (* This does not depends on the context hash.
       We can use the same function as I. *)
    I.compute_testchain_chain_id

  let compute_testchain_genesis =
    (* This does not depends on the context hash.
       We can use the same function as I. *)
    I.compute_testchain_genesis

  let commit_test_chain_genesis ~hash_override ctxt
      (forked_header : Block_header.t) =
    Log.lwt_debug "commit_test_chain_genesis" >>= fun () ->
    commit ~time:forked_header.shell.timestamp ctxt ~hash_override
    >>= fun (chash, _pch) ->
    let faked_shell_header : Block_header.shell_header =
      {
        forked_header.shell with
        proto_level = succ forked_header.shell.proto_level;
        predecessor = Tezos_crypto.Block_hash.zero;
        validation_passes = 0;
        operations_hash = Tezos_crypto.Operation_list_list_hash.empty;
        context = chash;
      }
    in
    let forked_block = Block_header.hash forked_header in
    let genesis_hash = compute_testchain_genesis forked_block in
    let chain_id = compute_testchain_chain_id genesis_hash in
    let genesis_header : Block_header.t =
      {
        shell = {faked_shell_header with predecessor = genesis_hash};
        protocol_data = Bytes.create 0;
      }
    in
    let _branch = get_branch chain_id in
    (* Store.Branch.set ctxt.index.repo branch commit *)
    Lwt.return genesis_header

  let clear_test_chain _index _chain_id =
    (* Icontext says: TODO remove commits... ??? *)
    Lwt.return ()

  let set_head _index _chain_id _commit = Lwt.return ()

  let set_master _ = force_exit "set_master"

  let find_predecessor_block_metadata_hash v =
    raw_find v current_predecessor_block_metadata_hash_key >>= function
    | None -> Lwt.return_none
    | Some data -> (
        match
          Data_encoding.Binary.of_bytes_opt Tezos_crypto.Block_metadata_hash.encoding data
        with
        | None ->
            lwt_failf
              "Unexpected error (Context.get_predecessor_block_metadata_hash)"
        | Some r -> Lwt.return_some r)

  let add_predecessor_block_metadata_hash v hash =
    let data =
      Data_encoding.Binary.to_bytes_exn Tezos_crypto.Block_metadata_hash.encoding hash
    in
    raw_add v current_predecessor_block_metadata_hash_key data

  let find_predecessor_ops_metadata_hash v =
    raw_find v current_predecessor_ops_metadata_hash_key >>= function
    | None -> Lwt.return_none
    | Some data -> (
        match
          Data_encoding.Binary.of_bytes_opt
            Tezos_crypto.Operation_metadata_list_list_hash.encoding
            data
        with
        | None ->
            lwt_failf
              "Unexpected error (Context.get_predecessor_ops_metadata_hash)"
        | Some r -> Lwt.return_some r)

  let add_predecessor_ops_metadata_hash v hash =
    let data =
      Data_encoding.Binary.to_bytes_exn
        Tezos_crypto.Operation_metadata_list_list_hash.encoding
        hash
    in
    raw_add v current_predecessor_ops_metadata_hash_key data

  (* -- Conversion ---------------------------------------- *)

  (* XXX convert may use lots of memory for better performance *)
  let convert pi ic =
    Log.lwt_log_notice "converting Irmin context to Plebeia..." >>= fun () ->
    let commit_info = I.commit_info ic in
    let option_pp f ppf = function
      | None -> Format.fprintf ppf "None"
      | Some x -> Format.fprintf ppf "Some (%a)" f x
    in
    Log.lwt_log_notice "converting Irmin %a parent: %a"
      Tezos_crypto.Context_hash.pp commit_info.hash
      (option_pp Tezos_crypto.Context_hash.pp) commit_info.parent
    >>= fun () ->
    (* Temporarily enable hashcons and node_cache *)
    let context_orig = Vc.context pi.pcontext in
    let node_cache = Some (P.Node_cache.(create ())) in
    let context = { context_orig with node_cache } in
    let fcur = F.empty context pi.pcontext.enc in
    let pc =
      {
        index = pi;
        parent = Option.map to_plebeia_commit_hash commit_info.parent;
        based_on = commit_info.parent;
        tree = ref fcur;
        based_cur = fcur;
        may_miss_parent = true;
      }
    in
    (* To reduce the memory usage, the contents of Cursor should be committed
       to the disk WITHOUT registring a root time to time,
       then forget on-memory-contents.

       Bud cache and small value cache must be cleaned.
    *)
    let flush pc =
      (* Write nodes to the disk *)
      let tree_at_root = F.top_tree !(pc.tree) in
      let tree, (_i, _hp) =
        (* It does NOT msync! *)
        fail_at_plebeia_error
        @@ F.write_top_tree tree_at_root
      in
      (* Then forget *)
      let tree = F.may_forget tree in
      pc.tree := tree
    in
    let cntr = ref 0 in
    (* XXX optimizable using trees *)
    I.raw_fold ic [] ~init:pc ~order:`Undefined ~f:(fun k t pc ->
        I.Tree.to_value t >>= function
        | None -> Lwt.return pc
        | Some v ->
            raw_add pc k v >>= fun pc ->
            incr cntr ;
            let open Lwt_syntax in
            let* () =
              if !cntr mod 50_000 = 0 then
                let* usage = Memstat.memory_usage () in
                Log.lwt_log_notice "conversion %d leaves: %a" !cntr Memstat.pp_memory_usage usage
              else Lwt.return_unit
            in
            if !cntr mod 200_000 = 0 then (
              Log.lwt_log_notice "%d leaves copied (%a)" !cntr pp_key k
              >>= fun () ->
              (* flushing.  not sure this is too frequent or too less *)
              flush pc ;
              Lwt.return pc)
            else Lwt.return pc)
    >>= fun pc ->
    Log.lwt_log_notice
      "converted Irmin context to Plebeia.  %d leaves copied"
      !cntr
    >>= fun () ->
    let nameenc = pi.pcontext.enc in
    let fix_tree tree =
      let Cursor (trail, node, _context, info) = F.get_raw_cursor tree in
      F.make
        (P.Cursor._Cursor (trail, node, context_orig, info)) nameenc []
    in
    let based_cur = fix_tree pc.based_cur in
    let tree = ref (fix_tree !(pc.tree)) in
    let pc = { pc with based_cur; tree } in
    Lwt.return pc

  let sync _idx =
    (* Reader sync is automatic in Plebeia *)
    Lwt.return_unit

  (* -- Extras --------------------------------------- *)

  let empty index =
    let fcur = F.empty (Vc.context index.pcontext) index.pcontext.enc in
    {
      index;
      parent = None;
      tree = ref fcur;
      based_cur = fcur;
      based_on = None;
      may_miss_parent = false;
    }

  (*-- Generic Store Primitives ------------------------------------------------*)

  (* let data_key key = current_data_key @ key *)

  type key = string list

  type value = bytes

  let force_exit = force_exit

  let parents t =
    match t.parent with
    | None -> []
    | Some ch -> [from_Ok @@ of_plebeia_commit_hash ch]

  let cursor ctxt = F.get_raw_cursor !(ctxt.tree)

  let to_context_hash = to_context_hash
end
