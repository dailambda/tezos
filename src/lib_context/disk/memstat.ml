type memory_usage =
  { rss_gb : float
  ; shared_rss_gb : float
  ; non_shared_rss_gb : float
  ; major_collections : int
  ; compactions : int
  }

let pp_memory_usage ppf { rss_gb; shared_rss_gb; non_shared_rss_gb; major_collections; compactions } =
  Format.fprintf ppf
    "RSS: %.05f GiB; Shared-RSS: %.05f GiB; \
     Non-shared: %.05f GiB; major_collections: %d ; compactions: %d"
    rss_gb
    shared_rss_gb
    non_shared_rss_gb
    major_collections
    compactions

let memory_usage () =
  let open Lwt_syntax in
  let stat = Gc.quick_stat () in
  let+ smaps = Smaps.get_self_smaps () in
  let smaps = Result.value ~default:[] smaps in
  let rss = Smaps.sum_rss smaps in
  let mmap_rss = Smaps.sum_shared_rss smaps in
  let rss_gb = Stdint.Uint64.to_float rss /. 1024. /. 1024. in
  let shared_rss_gb = Stdint.Uint64.to_float mmap_rss /. 1024. /. 1024. in
  let non_shared_rss_gb = rss_gb -. shared_rss_gb in
  { rss_gb
  ; shared_rss_gb
  ; non_shared_rss_gb
  ; major_collections = stat.major_collections
  ; compactions = stat.compactions
  }
