(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2018-2021 Nomadic Labs <contact@nomadic-labs.com>           *)
(* Copyright (c) 2018-2020 Tarides <contact@tarides.com>                     *)
(* Copyright (c) 2020 Metastate AG <hello@metastate.dev>                     *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Tezos - Versioned, block indexed (key x value) store *)

type error += Plebeia_error of string

(** The tree depth of a fold. See the [fold] function for more information. *)
type depth = Tezos_context_sigs.Context.depth

module type VIEW = sig
  (** The type for context views. *)
  type t

  (** The type for context keys. *)
  type key

  (** The type for context values. *)
  type value

  (** The type for context trees. *)
  type tree

  (** {2 Getters} *)

  (** [mem t k] is an Lwt promise that resolves to [true] iff [k] is bound
      to a value in [t]. *)
  val mem : t -> key -> bool Lwt.t

  (** [mem_tree t k] is like {!mem} but for trees. *)
  val mem_tree : t -> key -> bool Lwt.t

  (** [find t k] is an Lwt promise that resolves to [Some v] if [k] is
      bound to the value [v] in [t] and [None] otherwise. *)
  val find : t -> key -> value option Lwt.t

  (** [find_tree t k] is like {!find} but for trees. *)
  val find_tree : t -> key -> tree option Lwt.t

  (** [list t key] is the list of files and sub-nodes stored under [k] in [t].
      The result order is not specified but is stable.

      [offset] and [length] are used for pagination. *)
  val list :
    t -> ?offset:int -> ?length:int -> key -> (string * tree) list Lwt.t

  (** [length t key] is an Lwt promise that resolves to the number of
      files and sub-nodes stored under [k] in [t].

      It is equivalent to [let+ l = list t k in List.length l] but has a
      constant-time complexity. *)
  val length : t -> key -> int Lwt.t

  (** {2 Setters} *)

  (** [add t k v] is an Lwt promise that resolves to [c] such that:

    - [k] is bound to [v] in [c];
    - and [c] is similar to [t] otherwise.

    If [k] was already bound in [t] to a value that is physically equal
    to [v], the result of the function is a promise that resolves to
    [t]. Otherwise, the previous binding of [k] in [t] disappears. *)
  val add : t -> key -> value -> t Lwt.t

  (** [add_tree] is like {!add} but for trees. *)
  val add_tree : t -> key -> tree -> t Lwt.t

  (** [remove t k v] is an Lwt promise that resolves to [c] such that:

    - [k] is unbound in [c];
    - and [c] is similar to [t] otherwise. *)
  val remove : t -> key -> t Lwt.t

  (** {2 Folding} *)

  (** [fold ?depth t root ~order ~init ~f] recursively folds over the trees
      and values of [t]. The [f] callbacks are called with a key relative
      to [root]. [f] is never called with an empty key for values; i.e.,
      folding over a value is a no-op.

      The depth is 0-indexed. If [depth] is set (by default it is not), then [f]
      is only called when the conditions described by the parameter is true:

      - [Eq d] folds over nodes and values of depth exactly [d].
      - [Lt d] folds over nodes and values of depth strictly less than [d].
      - [Le d] folds over nodes and values of depth less than or equal to [d].
      - [Gt d] folds over nodes and values of depth strictly more than [d].
      - [Ge d] folds over nodes and values of depth more than or equal to [d].

      If [order] is [`Sorted] (the default), the elements are traversed in
      lexicographic order of their keys. For large nodes, it is memory-consuming,
      use [`Undefined] for a more memory efficient [fold]. *)
  val fold :
    ?depth:depth ->
    t ->
    key ->
    order:[`Sorted | `Undefined] ->
    init:'a ->
    f:(key -> tree -> 'a -> 'a Lwt.t) ->
    'a Lwt.t
end

module Kind = Tezos_context_sigs.Context.Kind

module type TREE = sig
  (** [Tree] provides immutable, in-memory partial mirror of the
      context, with lazy reads and delayed writes. The trees are Merkle
      trees that carry the same hash as the part of the context they
      mirror.

      Trees are immutable and non-persistent (they disappear if the
      host crash), held in memory for efficiency, where reads are done
      lazily and writes are done only when needed, e.g. on
      [Context.commit]. If a key is modified twice, only the last
      value will be written to disk on commit. *)

  (** The type for context views. *)
  type t

  (** The type for context trees. *)
  type tree

  include VIEW with type t := tree and type tree := tree

  (** [empty _] is the empty tree. *)
  val empty : t -> tree

  (** [is_empty t] is true iff [t] is [empty _]. *)
  val is_empty : tree -> bool

  (** [kind t] is [t]'s kind. It's either a tree node or a leaf
      value. *)
  val kind : tree -> Kind.t

  (** [to_value t] is an Lwt promise that resolves to [Some v] if [t]
      is a leaf tree and [None] otherwise. It is equivalent to [find t
      []]. *)
  val to_value : tree -> value option Lwt.t

  (** [of_value _ v] is an Lwt promise that resolves to the leaf tree
      [v]. Is is equivalent to [add (empty _) [] v]. *)
  val of_value : t -> value -> tree Lwt.t

  (** [hash t] is [t]'s Merkle hash. *)
  val hash : tree -> Tezos_crypto.Context_hash.t

  (** [equal x y] is true iff [x] and [y] have the same Merkle hash. *)
  val equal : tree -> tree -> bool

  (** {2 Caches} *)

  (** [clear ?depth t] clears all caches in the tree [t] for subtrees with a
      depth higher than [depth]. If [depth] is not set, all of the subtrees are
      cleared. *)
  val clear : ?depth:int -> tree -> unit
end

(** [TEZOS_CONTEXT] is the module type implemented by all storage
    implementations. This is the module type that the {e shell} expects for its
    operation. As such, it should be a strict superset of the interface exposed
    to the protocol (see module type {!S} above and
    {!Tezos_protocol_environment.Environment_context_intf.S}).

    The main purpose of this module type is to keep the on-disk and in-memory
    implementations in sync.
*)
module type TEZOS_CONTEXT = sig
  (** {2 Generic interface} *)

  (** A block-indexed (key x value) store directory.  *)
  type index

  include VIEW with type key = string list and type value = bytes

  (* TODO
     module Proof : PROOF

     type node_key

     type value_key

     (** The type of references to tree objects annotated with the type of that
         object (either a value or a node). Used to build a shallow tree with
         {!Tree.shallow} *)
     type kinded_key = [`Node of node_key | `Value of value_key]
  *)

  module Tree : sig
    include
      TREE
        with type t := t
         and type key := key
         and type value := value
         and type tree := tree

    (** [pp] is the pretty-printer for trees. *)
    val pp : Format.formatter -> tree -> unit

    (** {2 Data Encoding} *)

    (** The type for in-memory, raw contexts. *)
    type raw = [`Value of bytes | `Tree of raw String.Map.t]

    (** [raw_encoding] is the data encoding for raw trees. *)
    val raw_encoding : raw Data_encoding.t

    (** [to_raw t] is an Lwt promise that resolves to a raw tree
        equivalent to [t]. *)
    val to_raw : tree -> raw Lwt.t

    (** [of_raw t] is the tree equivalent to the raw tree [t]. *)
    val of_raw : raw -> tree

    (*
    (** [unshallow t] is the tree equivalent to [t] but with all subtrees evaluated,
        i.e. without "reference" nodes.
        Concretely, it means that no node in [t] contains just an in-memory hash,
        but the actual value instead. *)
    val unshallow : tree -> tree Lwt.t
*)

    type repo

    val make_repo : unit -> repo Lwt.t

    (*
    (** [shallow repo k] is the "shallow" tree having key [k] based on the
        repository [repo]. A shallow tree is a tree that exists in an underlying
        backend repository, but has not yet been loaded into memory from that
        backend. *)
    val shallow : repo -> kinded_key -> tree

    val is_shallow : tree -> bool

    val kinded_key : tree -> kinded_key option
*)
  end

  (*
  (** [produce r h f] runs [f] on top of a real store [r], producing a proof and
      a result using the initial root hash [h].

      The trees produced during [f]'s computation will carry the full history of
      reads. This history will be reset when [f] is complete so subtrees
      escaping the scope of [f] will not cause memory leaks.

      Calling [produce_proof] recursively has an undefined behaviour. *)
  type ('proof, 'result) producer :=
    index ->
    kinded_key ->
    (tree -> (tree * 'result) Lwt.t) ->
    ('proof * 'result) Lwt.t

  (** [verify p f] runs [f] in checking mode. [f] is a function that takes a
      tree as input and returns a new version of the tree and a result. [p] is a
      proof, that is a minimal representation of the tree that contains what [f]
      should be expecting.

      Therefore, contrary to trees found in a storage, the contents of the trees
      passed to [f] may not be available. For this reason, looking up a value at
      some [path] can now produce three distinct outcomes:
      - A value [v] is present in the proof [p] and returned : [find tree path]
        is a promise returning [Some v];
      - [path] is known to have no value in [tree] : [find tree path] is a
        promise returning [None]; and
      - [path] is known to have a value in [tree] but [p] does not provide it
        because [f] should not need it: [verify] returns an error classifying
        [path] as an invalid path (see below).

      The same semantics apply to all operations on the tree [t] passed to [f]
      and on all operations on the trees built from [f].

      The generated tree is the tree after [f] has completed. That tree is
      disconnected from any storage (i.e. [index]). It is possible to run
      operations on it as long as they don't require loading shallowed subtrees.

      The result is [Error (`Msg _)] if the proof is rejected:
      - For tree proofs: when [p.before] is different from the hash of
        [p.state];
      - For tree and stream proofs: when [p.after] is different from the hash
        of [f p.state];
      - For tree proofs: when [f p.state] tries to access invalid paths in
        [p.state];
      - For stream proofs: when the proof is not consumed in the exact same
        order it was produced;
      - For stream proofs: when the proof is too short or not empty once [f] is
        done.

      @raise Failure if the proof version is invalid or incompatible with the
      verifier. *)
  type ('proof, 'result) verifier :=
    'proof ->
    (tree -> (tree * 'result) Lwt.t) ->
    ( tree * 'result,
      [ `Proof_mismatch of string
      | `Stream_too_long of string
      | `Stream_too_short of string ] )
    result
    Lwt.t

  (** The type for tree proofs.

      Guarantee that the given computation performs exactly the same state
      operations as the generating computation, *in some order*. *)
  type tree_proof := Proof.tree Proof.t

  (** [produce_tree_proof] is the producer of tree proofs. *)
  val produce_tree_proof : (tree_proof, 'a) producer

  (** [verify_tree_proof] is the verifier of tree proofs. *)
  val verify_tree_proof : (tree_proof, 'a) verifier

  (** The type for stream proofs.

      Guarantee that the given computation performs exactly the same state
      operations as the generating computation, in the exact same order. *)
  type stream_proof := Proof.stream Proof.t

  (** [produce_stream_proof] is the producer of stream proofs. *)
  val produce_stream_proof : (stream_proof, 'a) producer

  (** [verify_stream] is the verifier of stream proofs. *)
  val verify_stream_proof : (stream_proof, 'a) verifier
*)

  type context = t

  (** [memory_context_tree] is a forward declaration of the type of
      an in-memory Irmin tree. This type variable is to be substituted
      by a concrete type wherever the {!TEZOS_CONTEXT} signature is used. *)
  type memory_context_tree

  val index : context -> index

  (** Open or initialize a versioned store at a given path.

      @param indexing_strategy determines whether newly-exported objects by
      this store handle should also be added to the store's index. [`Minimal]
      (the default) only adds objects to the index when they are {i commits},
      whereas [`Always] indexes every object type. The indexing strategy used
      for existing stores can be changed without issue (as only {i
      newly}-exported objects are impacted). *)
  val init :
    ?patch_context:(context -> context tzresult Lwt.t) ->
    ?readonly:bool ->
    (*
    ?indexing_strategy:[`Always | `Minimal] ->
    ?index_log_size:int ->
*)
    string ->
    index Lwt.t

  (** Close the index. Does not fail when the context is already closed. *)
  val close : index -> unit Lwt.t

  val compute_testchain_chain_id : Tezos_crypto.Block_hash.t -> Tezos_crypto.Chain_id.t

  val compute_testchain_genesis : Tezos_crypto.Block_hash.t -> Tezos_crypto.Block_hash.t

  (** Build an empty context from an index. The resulting context should not
      be committed. *)
  val empty : index -> t

  (*
  (** Returns [true] if the context is empty. *)
  val is_empty : t -> bool
*)

  val commit_genesis :
    hash_override:Tezos_crypto.Context_hash.t option ->
    index ->
    chain_id:Tezos_crypto.Chain_id.t ->
    time:Time.Protocol.t ->
    protocol:Tezos_crypto.Protocol_hash.t ->
    (Tezos_crypto.Context_hash.t * Plebeia.Hash.Prefix.t) tzresult Lwt.t

  val commit_test_chain_genesis :
    hash_override:Tezos_crypto.Context_hash.t option ->
    t ->
    Block_header.t ->
    Block_header.t Lwt.t

  (*
  (** Extract a subtree from the {!Tezos_context.Context.t} argument and returns
      it as a {!Tezos_context_memory.Context.tree} (note the the type change!). **)
  val to_memory_tree : t -> string list -> memory_context_tree option Lwt.t
*)

  (*
  (** [merkle_tree t leaf_kind key] returns a Merkle proof for [key] (i.e.
    whose hashes reach [key]). If [leaf_kind] is [Block_services.Hole], the value
    at [key] is a hash. If [leaf_kind] is [Block_services.Raw_context],
    the value at [key] is a [Block_services.raw_context]. Values higher
    in the returned tree are hashes of the siblings on the path to
    reach [key]. *)
  val merkle_tree :
    t -> Proof_types.merkle_leaf_kind -> key -> Proof_types.merkle_tree Lwt.t

  (** [merkle_tree_v2 t leaf_kind key] returns an Irmin Merkle proof for [key] (i.e.
    a proof that *something* is in the context at [key]).
    The proof is supposed to be produced by Irmin's [produce_proof], and consumed
    by Irmin's [verify_proof]. The value embedded in the proof depends on [leaf_kind].
    If [leaf_kind] is [Block_services.Raw_context], the embeded value is the complete
    subtree in the context at [key].
    If [leaf_kind] is [Block_services.Hole], the embedded value is the hash of the
    subtree described above. *)
  val merkle_tree_v2 :
    t -> Proof_types.merkle_leaf_kind -> key -> Proof.tree Proof.t Lwt.t
*)

  (** {2 Accessing and Updating Versions} *)

  val exists : index -> Tezos_crypto.Context_hash.t -> bool Lwt.t

  val checkout : index -> Tezos_crypto.Context_hash.t -> context option Lwt.t

  val checkout_exn : index -> Tezos_crypto.Context_hash.t -> context Lwt.t

  val hash : time:Time.Protocol.t -> ?message:string -> t -> Tezos_crypto.Context_hash.t

  val commit :
    hash_override:Tezos_crypto.Context_hash.t option ->
    time:Time.Protocol.t ->
    ?message:string ->
    t ->
    (Tezos_crypto.Context_hash.t * Plebeia.Hash.Prefix.t) Lwt.t

  (*
  (** [gc t h] removes from disk all the data older than the commit
    [hash]. Every operations working on checkouts greater or equal to
    [h] will continue to work. Calling [checkout h'] on GC-ed commits
    will return [None]. *)
  val gc : index -> Tezos_crypto.Context_hash.t -> unit Lwt.t
*)

  (** Sync the context with disk. Only useful for read-only instances.
      Does not fail when the context is not in read-only mode. *)
  val sync : index -> unit Lwt.t

  (*
  (** [is_gc_allowed index] returns whether or not it is possible to
     run a GC on the given context tree. If false is returned, it
     means that the context was run, at least once, with the indexing
     strategy mode "always", which is not suitable for GC.*)
  val is_gc_allowed : index -> bool
*)

  val set_head : index -> Tezos_crypto.Chain_id.t -> Tezos_crypto.Context_hash.t -> unit Lwt.t

  val set_master : index -> Tezos_crypto.Context_hash.t -> unit Lwt.t

  (** {2 Hash version} *)

  (** Get the hash version used for the context *)
  val get_hash_version : context -> Tezos_crypto.Context_hash.Version.t

  (** Set the hash version used for the context.  It may recalculate the hashes
    of the whole context, which can be a long process.
    Returns an [Error] if the hash version is unsupported. *)
  val set_hash_version :
    context -> Tezos_crypto.Context_hash.Version.t -> context tzresult Lwt.t

  (** {2 Predefined Fields} *)

  val get_protocol : context -> Tezos_crypto.Protocol_hash.t Lwt.t

  val add_protocol : context -> Tezos_crypto.Protocol_hash.t -> context Lwt.t

  val get_test_chain : context -> Test_chain_status.t Lwt.t

  val add_test_chain : context -> Test_chain_status.t -> context Lwt.t

  val remove_test_chain : context -> context Lwt.t

  val fork_test_chain :
    context ->
    protocol:Tezos_crypto.Protocol_hash.t ->
    expiration:Time.Protocol.t ->
    context Lwt.t

  val clear_test_chain : index -> Tezos_crypto.Chain_id.t -> unit Lwt.t

  val find_predecessor_block_metadata_hash :
    context -> Tezos_crypto.Block_metadata_hash.t option Lwt.t

  val add_predecessor_block_metadata_hash :
    context -> Tezos_crypto.Block_metadata_hash.t -> context Lwt.t

  val find_predecessor_ops_metadata_hash :
    context -> Tezos_crypto.Operation_metadata_list_list_hash.t option Lwt.t

  val add_predecessor_ops_metadata_hash :
    context -> Tezos_crypto.Operation_metadata_list_list_hash.t -> context Lwt.t

  (*
  val retrieve_commit_info :
    index ->
    Block_header.t ->
    (Protocol_hash.t
    * string
    * string
    * Time.Protocol.t
    * Test_chain_status.t
    * Tezos_crypto.Context_hash.t
    * Block_metadata_hash.t option
    * Operation_metadata_list_list_hash.t option
    * Tezos_crypto.Context_hash.t list)
    tzresult
    Lwt.t

  val check_protocol_commit_consistency :
    expected_context_hash:Tezos_crypto.Context_hash.t ->
    given_protocol_hash:Protocol_hash.t ->
    author:string ->
    message:string ->
    timestamp:Time.Protocol.t ->
    test_chain_status:Test_chain_status.t ->
    predecessor_block_metadata_hash:Block_metadata_hash.t option ->
    predecessor_ops_metadata_hash:Operation_metadata_list_list_hash.t option ->
    data_merkle_root:Tezos_crypto.Context_hash.t ->
    parents_contexts:Tezos_crypto.Context_hash.t list ->
    bool Lwt.t
*)

  val parents : t -> Tezos_crypto.Context_hash.t list

  val raw_fold :
    ?depth:depth ->
    t ->
    string list ->
    init:'a ->
    f:(string list -> tree -> 'a -> 'a Lwt.t) ->
    order:[`Sorted | `Undefined] ->
    'a Lwt.t
end

module type TEZOS_CONTEXT_UNIX = sig
  type error +=
    | Cannot_create_file of string
    | Cannot_open_file of string
    | Cannot_retrieve_commit_info of Tezos_crypto.Context_hash.t
    | Cannot_find_protocol
    | Suspicious_file of int

  include
    TEZOS_CONTEXT
      with type memory_context_tree := Tezos_context_memory.Context.tree

  (** Sync the context with disk. Only useful for read-only instances.
      Does not fail when the context is not in read-only mode. *)
  val sync : index -> unit Lwt.t

  (*
  (** An Irmin context corresponds to an in-memory overlay (corresponding
      to the type {!tree}) over some on-disk data. Writes are buffered in
      the overlay temporarily. Calling [flush] performs these writes on
      disk and returns a context with an empty overlay. *)
  val flush : t -> t Lwt.t
*)

  (** {2 Context dumping} *)

  (*
  (** [dump_context] is used to export snapshots of the context at given hashes. *)
  val dump_context :
    index ->
    Tezos_crypto.Context_hash.t ->
    fd:Lwt_unix.file_descr ->
    on_disk:bool ->
    progress_display_mode:Animation.progress_display_mode ->
    int tzresult Lwt.t

  (** Rebuild a context from a given snapshot. *)
  val restore_context :
    index ->
    expected_context_hash:Tezos_crypto.Context_hash.t ->
    nb_context_elements:int ->
    fd:Lwt_unix.file_descr ->
    legacy:bool ->
    in_memory:bool ->
    progress_display_mode:Animation.progress_display_mode ->
    unit tzresult Lwt.t

  (** Offline integrity checking and statistics for contexts. *)
  module Checks : sig
    module Pack : Irmin_pack_unix.Checks.S

    module Index : Index.Checks.S
  end
*)
end
