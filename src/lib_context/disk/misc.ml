module Option = struct
  include Option

  let pp f ppf = function
    | None -> Format.fprintf ppf "None"
    | Some x -> Format.fprintf ppf "@[<2>Some@ (%a)@]" f x
end

module Bytes = struct
  include Bytes

  let pp ppf b = Format.fprintf ppf "%S" (Bytes.unsafe_to_string b)
end

let pp_with_encoding = Plebeia.Data_encoding_tools.pp_with_encoding
