(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019-2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Legacy_monad_globals

(** Tezos - Versioned, block indexed (key x value) store *)

module Log = Internal_event.Legacy_logging.Make (struct
  let name = "xcontext"
end)

let mode =
  match Sys.getenv "CONTEXT_SUBSYSTEM" with
  | "Both" -> `Both
  | "PlebeiaOnly" ->
      Log.fatal_error "Configuration: PlebeiaOnly" ;
      `PlebeiaOnly
  | "IrminOnly" ->
      Log.fatal_error "Configuration: IrminOnly" ;
      `IrminOnly
  | _ -> assert false
  | exception _ -> `Both

(*
let do_disk_memory_prof =
  match Sys.getenv "CONTEXT_SUBSYSTEM_DISK_MEMORY_PROF" with
  | "" -> false
  | _ -> true
  | exception Not_found -> false
*)

module Make (Encoding : module type of Tezos_context_encoding.Context) = struct
  module I = Context.Make (Encoding)
  module P = Pcontext.Make (Encoding)

  type error +=
    | Cannot_create_file = I.Cannot_create_file
    | Cannot_open_file = I.Cannot_open_file
    | Cannot_find_protocol = I.Cannot_find_protocol
    | Suspicious_file = I.Suspicious_file

  (** A (key x value) store for a given block. *)
  type t = {ic : I.t option; pc : P.t option; index : index}

  and context = t

  (** A block-indexed (key x value) store directory.  *)
  and index = {
    ii : I.index option;
    pi : (P.index * (unit -> I.index Lwt.t)) option;
    hacked_patch_context : context -> context tzresult Lwt.t;
  }

  (* XXX never used *)
  let _pp_index =
    let ii2s = function Some _ -> "Some" | None -> "None" in
    let pi2s = function Some _ -> "Some" | None -> "None" in
    fun fmt {ii; pi; _} ->
      Format.fprintf fmt "{ii=%s;pi=%s}" (ii2s ii) (pi2s pi)

  type tree = {itree : I.tree option; ptree : P.tree option}

  let index c = c.index

  let with_temp_icontext iif f =
    let open Lwt_syntax in
    let* ii = iif () in
    let* res = f ii in
    let+ () = I.close ii in
    res

  let lwt_failf fmt = Format.kasprintf (fun s -> Lwt.fail (Failure s)) fmt

  type key = string list

  let string_of_key key = String.concat "/" @@ ("" :: key)

  type value = Bytes.t

  (** Open or initialize a versioned store at a given path. *)
  let init ?patch_context:original_patch_context ?readonly ?indexing_strategy
      ?index_log_size context_dir =
    (* [patch_context] is used only at [commit_genesis] against
       the empty context.  This code assumes this property. *)
    let patched_context = ref (None : context option) in
    let hacked_patch_context c =
      let open Lwt_result_syntax in
      assert (!patched_context = None) ;
      let* c =
        match original_patch_context with None -> return c | Some f -> f c
      in
      patched_context := Some c ;
      return c
    in
    let i_init () =
      I.init
        ~patch_context:(fun _ ->
          match !patched_context with
          | Some {ic = Some ic; _} -> return ic
          | _ -> assert false)
        ?readonly
        ?indexing_strategy
        ?index_log_size
        context_dir
    in
    let p_init () =
      P.init
        ~patch_context:(fun _ ->
          match !patched_context with
          | Some {pc = Some pc; _} -> return pc
          | _ -> assert false)
        ?readonly
        context_dir
    in
    let open Lwt_syntax in
    match mode with
    | `Both ->
        let* ii = i_init () in
        let+ pi = p_init () in
        let pi = (pi, fun () -> Lwt.return ii) in
        {ii = Some ii; pi = Some pi; hacked_patch_context}
    | `IrminOnly ->
        let+ ii = i_init () in
        {ii = Some ii; pi = None; hacked_patch_context}
    | `PlebeiaOnly ->
        let+ pi = p_init () in
        let pi = (pi, i_init) in
        {ii = None; pi = Some pi; hacked_patch_context}

  let do_index fi fp {ii; pi; _} =
    let open Lwt_syntax in
    let* () = match ii with None -> Lwt.return_unit | Some ii -> fi ii in
    match pi with None -> Lwt.return_unit | Some (pi, _) -> fp pi

  let close = do_index I.close P.close

  (* The both versions are the same *)
  let compute_testchain_chain_id = I.compute_testchain_chain_id

  (* The both versions are the same *)
  let compute_testchain_genesis = I.compute_testchain_genesis

  let inconsistent_exit fmt =
    Format.kasprintf (fun s -> P.force_exit "inconsistent: %s" s) fmt

  let commit_test_chain_genesis context bh =
    let {ic; pc; _} = context in
    match (ic, pc) with
    | None, None -> assert false
    | None, Some pc -> P.commit_test_chain_genesis pc bh ~hash_override:None
    | Some ic, None -> I.commit_test_chain_genesis ic bh
    | Some ic, Some pc ->
        let open Lwt_syntax in
        let* bh1 = I.commit_test_chain_genesis ic bh in
        let ch1 = bh1.shell.context in
        let+ bh2 =
          P.commit_test_chain_genesis pc bh ~hash_override:(Some ch1)
        in
        if bh1 <> bh2 then inconsistent_exit "commit_test_chain_genesis" ;
        bh2

  let contexts context if_ pf merge =
    let open Lwt_syntax in
    let {ic; pc; _} = context in
    match (ic, pc) with
    | None, None -> merge (None, None)
    | Some ic, None ->
        let* ires = if_ ic in
        merge (Some ires, None)
    | None, Some pc ->
        let* pres = pf pc in
        merge (None, Some pres)
    | Some ic, Some pc ->
        let* ires = if_ ic in
        let* pres = pf pc in
        merge (Some ires, Some pres)

  let do_contexts context if_ pf check =
    contexts context if_ pf @@ function
    | None, None -> assert false
    | Some ires, None -> Lwt.return ires
    | None, Some pres -> Lwt.return pres
    | Some ires, Some pres ->
        check ires pres ;
        Lwt.return ires

  let map_contexts context if_ pf =
    contexts context if_ pf @@ function
    | None, None -> assert false
    | Some ic, None -> Lwt.return {context with ic = Some ic}
    | None, Some pc -> Lwt.return {context with pc = Some pc}
    | Some ic, Some pc -> Lwt.return {context with ic = Some ic; pc = Some pc}

  let mem context key =
    do_contexts
      context
      (fun ic -> I.mem ic key)
      (fun pc -> P.mem pc key)
      (fun b1 b2 -> if b1 <> b2 then inconsistent_exit "mem")

  let mem_tree context key =
    do_contexts
      context
      (fun ic -> I.mem_tree ic key)
      (fun pc -> P.mem_tree pc key)
      (fun b1 b2 -> if b1 <> b2 then inconsistent_exit "mem_tree")

  let find context key =
    do_contexts
      context
      (fun ic -> I.find ic key)
      (fun pc -> P.find pc key)
      (fun res1 res2 ->
        if res1 <> res2 then
          inconsistent_exit
            "find %s i=%S p=%S"
            (string_of_key key)
            (match res1 with None -> "NONE" | Some v -> Bytes.to_string v)
            (match res2 with None -> "NONE" | Some v -> Bytes.to_string v))

  let find_tree context key =
    let open Lwt_syntax in
    let {ic; pc; _} = context in
    match (ic, pc) with
    | None, None -> assert false
    | Some ic, None -> (
        I.find_tree ic key >|= function
        | None -> None
        | Some itree -> Some {itree = Some itree; ptree = None})
    | None, Some pc -> (
        P.find_tree pc key >|= function
        | None -> None
        | Some ptree -> Some {itree = None; ptree = Some ptree})
    | Some ic, Some pc -> (
        let* ires = I.find_tree ic key in
        let+ pres = P.find_tree pc key in
        match (ires, pres) with
        | None, None -> None
        | Some it, Some pt -> Some {itree = Some it; ptree = Some pt}
        | ires, pres ->
            inconsistent_exit
              "find_tree %s i=%S p=%S"
              (string_of_key key)
              (match ires with None -> "NONE" | Some _ -> "SOME _")
              (match pres with None -> "NONE" | Some _ -> "SOME _"))

  let list {ic; pc; _} ?offset ?length dir =
    match (ic, pc) with
    | None, None -> assert false
    | Some ic, None ->
        I.list ic ?offset ?length dir
        >|= List.map (fun (k, itree) -> (k, {itree = Some itree; ptree = None}))
    | None, Some pc ->
        P.list pc ?offset ?length dir
        >|= List.map (fun (k, ptree) -> (k, {ptree = Some ptree; itree = None}))
    | Some ic, Some pc ->
        (* I guess we cannot really assure the ordering of list of the both *)
        I.list ic ?offset ?length dir
        >>= Lwt_list.map_s (fun (k, itree) ->
                P.find_tree pc (dir @ [k]) >|= function
                | None ->
                    inconsistent_exit
                      "list: %s, %s"
                      (string_of_key dir)
                      (string_of_key [k])
                | Some ptree -> (k, {itree = Some itree; ptree = Some ptree}))

  (* writes *)

  let add context key v =
    map_contexts context (fun ic -> I.add ic key v) (fun pc -> P.add pc key v)

  let add_tree context key tree =
    let open Lwt_syntax in
    match (context, tree) with
    | {ic = None; pc = None; _}, _ -> assert false
    | {ic = Some ic; pc = None; _}, {itree = Some itree; ptree = None} ->
        let+ ic = I.add_tree ic key itree in
        {context with ic = Some ic}
    | {ic = None; pc = Some pc; _}, {itree = None; ptree = Some ptree} ->
        let+ pc = P.add_tree pc key ptree in
        {context with pc = Some pc}
    | {ic = Some ic; pc = Some pc; _}, {itree = Some itree; ptree = Some ptree}
      ->
        let* ic = I.add_tree ic key itree in
        let+ pc = P.add_tree pc key ptree in
        {context with ic = Some ic; pc = Some pc}
    | {ic; pc; _}, {itree; ptree} ->
        inconsistent_exit
          "add_tree %s i=%S p=%S itree=%S ptree=%S"
          (string_of_key key)
          (match ic with None -> "NONE" | Some _ -> "SOME _")
          (match pc with None -> "NONE" | Some _ -> "SOME _")
          (match itree with None -> "NONE" | Some _ -> "SOME _")
          (match ptree with None -> "NONE" | Some _ -> "SOME _")

  let remove context key =
    map_contexts context (fun ic -> I.remove ic key) (fun pc -> P.remove pc key)

  let fold ?depth context key ~order ~init ~f =
    let {ic; pc; _} = context in
    match (ic, pc) with
    | None, None -> assert false
    | Some ic, None ->
        I.fold ?depth ic key ~order ~init ~f:(fun k t a ->
            f k {itree = Some t; ptree = None} a)
    | None, Some pc ->
        P.fold ?depth pc key ~order ~init ~f:(fun k t a ->
            f k {itree = None; ptree = Some t} a)
    | Some ic, Some pc ->
        let open Lwt_syntax in
        (* we cannot do more checks than comparing the targets *)
        let* iitems =
          I.fold ?depth ic key ~order ~init:[] ~f:(fun x _ xs ->
              Lwt.return @@ (x :: xs))
        in
        let* pitems =
          P.fold ?depth pc key ~order ~init:[] ~f:(fun x _ xs ->
              Lwt.return @@ (x :: xs))
        in
        if iitems <> pitems then (
          Format.eprintf "folding: %s@." (string_of_key key) ;
          Format.eprintf "I@." ;
          List.iter (fun x -> prerr_endline @@ string_of_key x) iitems ;
          Format.eprintf "P@." ;
          List.iter (fun x -> prerr_endline @@ string_of_key x) pitems ;
          inconsistent_exit "fold: %s" (string_of_key key)) ;
        (* We fold only using I.  If we do also for P, we fold twice. *)
        I.fold ?depth ic key ~order ~init ~f:(fun k t a ->
            P.find_tree pc (key @ k) >>= function
            | None ->
                inconsistent_exit
                  "fold, P.find_tree: %s, %s"
                  (string_of_key key)
                  (string_of_key k)
            | Some ptree -> f k {itree = Some t; ptree = Some ptree} a)

  module Tree = struct
    let pp ppf {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.pp ppf itree
      | None, Some ptree -> P.Tree.pp ppf ptree
      | Some itree, Some ptree ->
          Format.fprintf ppf "<%a,%a>" I.Tree.pp itree P.Tree.pp ptree

    let mem {itree; ptree} key =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.mem itree key
      | None, Some ptree -> P.Tree.mem ptree key
      | Some itree, Some ptree ->
          let open Lwt_syntax in
          let* ib = I.Tree.mem itree key in
          let+ pb = P.Tree.mem ptree key in
          if ib = pb then ib
          else inconsistent_exit "Tree.mem: %s" (string_of_key key)

    let mem_tree {itree; ptree} key =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.mem_tree itree key
      | None, Some ptree -> P.Tree.mem_tree ptree key
      | Some itree, Some ptree ->
          let open Lwt_syntax in
          let* ib = I.Tree.mem_tree itree key in
          let+ pb = P.Tree.mem_tree ptree key in
          if ib = pb then ib
          else inconsistent_exit "Tree.mem_tree: %s" (string_of_key key)

    let find {itree; ptree} key =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.find itree key
      | None, Some ptree -> P.Tree.find ptree key
      | Some itree, Some ptree ->
          let open Lwt_syntax in
          let* iv = I.Tree.find itree key in
          let+ pv = P.Tree.find ptree key in
          if iv = pv then iv
          else
            inconsistent_exit
              "Tree.find: %s %S %S"
              (string_of_key key)
              (match iv with None -> "NONE" | Some v -> Bytes.to_string v)
              (match pv with None -> "NONE" | Some v -> Bytes.to_string v)

    let find_tree {itree; ptree} key =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> (
          I.Tree.find_tree itree key >|= function
          | None -> None
          | Some _ as itree -> Some {itree; ptree = None})
      | None, Some ptree -> (
          P.Tree.find_tree ptree key >|= function
          | None -> None
          | Some _ as ptree -> Some {ptree; itree = None})
      | Some itree, Some ptree -> (
          let open Lwt_syntax in
          let* itreeo = I.Tree.find_tree itree key in
          let+ ptreeo = P.Tree.find_tree ptree key in
          match (itreeo, ptreeo) with
          | Some itree, Some ptree ->
              Some {itree = Some itree; ptree = Some ptree}
          | None, None -> None
          | _ ->
              inconsistent_exit
                "Tree.find_tree: %s %S %S"
                (string_of_key key)
                (match itreeo with None -> "NONE" | Some _ -> "SOME")
                (match ptreeo with None -> "NONE" | Some _ -> "SOME"))

    let list {itree; ptree} ?offset ?length dir =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None ->
          I.Tree.list itree ?offset ?length dir
          >|= List.map (fun (k, itree) ->
                  (k, {itree = Some itree; ptree = None}))
      | None, Some ptree ->
          P.Tree.list ptree ?offset ?length dir
          >|= List.map (fun (k, ptree) ->
                  (k, {ptree = Some ptree; itree = None}))
      | Some itree, Some ptree ->
          (* I guess we cannot really assure the ordering of list of the both *)
          I.Tree.list itree ?offset ?length dir
          >>= Lwt_list.map_s (fun (k, itree) ->
                  P.Tree.find_tree ptree (dir @ [k]) >|= function
                  | None ->
                      inconsistent_exit
                        "Tree.list: %s, %s"
                        (string_of_key dir)
                        (string_of_key [k])
                  | Some ptree -> (k, {itree = Some itree; ptree = Some ptree}))

    (** {2 Setters} *)

    let add_tree {itree; ptree} key {itree = itree'; ptree = ptree'} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> (
          match (itree', ptree') with
          | Some itree', None ->
              I.Tree.add_tree itree key itree' >|= fun itree ->
              {itree = Some itree; ptree = None}
          | _ -> assert false)
      | None, Some ptree -> (
          match (itree', ptree') with
          | None, Some ptree' ->
              P.Tree.add_tree ptree key ptree' >|= fun ptree ->
              {itree = None; ptree = Some ptree}
          | _ -> assert false)
      | Some itree, Some ptree -> (
          match (itree', ptree') with
          | Some itree', Some ptree' ->
              let open Lwt_syntax in
              let* itree = I.Tree.add_tree itree key itree' in
              let+ ptree = P.Tree.add_tree ptree key ptree' in
              {itree = Some itree; ptree = Some ptree}
          | _ -> assert false)

    let add {itree; ptree} key v =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None ->
          I.Tree.add itree key v >|= fun itree ->
          {itree = Some itree; ptree = None}
      | None, Some ptree ->
          P.Tree.add ptree key v >|= fun ptree ->
          {itree = None; ptree = Some ptree}
      | Some itree, Some ptree ->
          let open Lwt_syntax in
          let* itree = I.Tree.add itree key v in
          let+ ptree = P.Tree.add ptree key v in
          {itree = Some itree; ptree = Some ptree}

    let remove {itree; ptree} key =
      let open Lwt_syntax in
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None ->
          let+ itree = I.Tree.remove itree key in
          {itree = Some itree; ptree = None}
      | None, Some ptree ->
          let+ ptree = P.Tree.remove ptree key in
          {itree = None; ptree = Some ptree}
      | Some itree, Some ptree ->
          let* itree = I.Tree.remove itree key in
          let+ ptree = P.Tree.remove ptree key in
          {itree = Some itree; ptree = Some ptree}

    let fold ?depth {itree; ptree} key ~order ~init ~f =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None ->
          I.Tree.fold ?depth itree key ~order ~init ~f:(fun k t a ->
              f k {itree = Some t; ptree = None} a)
      | None, Some ptree ->
          P.Tree.fold ?depth ptree key ~order ~init ~f:(fun k t a ->
              f k {itree = None; ptree = Some t} a)
      | Some itree, Some ptree ->
          (* we cannot do more checks than comparing the targets *)
          let open Lwt_syntax in
          let* iitems =
            I.Tree.fold ?depth itree key ~order ~init:[] ~f:(fun x _ xs ->
                Lwt.return @@ (x :: xs))
          in
          let* pitems =
            P.Tree.fold ?depth ptree key ~order ~init:[] ~f:(fun x _ xs ->
                Lwt.return @@ (x :: xs))
          in
          if iitems <> pitems then (
            Format.eprintf "folding: %s@." (string_of_key key) ;
            Format.eprintf "I@." ;
            List.iter (fun x -> prerr_endline @@ string_of_key x) iitems ;
            Format.eprintf "P@." ;
            List.iter (fun x -> prerr_endline @@ string_of_key x) pitems ;
            inconsistent_exit "fold: %s" (string_of_key key)) ;
          (* We fold only using I.  If we do also for P, we fold twice. *)
          I.Tree.fold ?depth itree key ~order ~init ~f:(fun k t a ->
              P.Tree.find_tree ptree (key @ k) >>= function
              | None ->
                  inconsistent_exit
                    "fold: P.Tree.find_tree: %s, %s"
                    (string_of_key key)
                    (string_of_key k)
              | Some ptree -> f k {itree = Some t; ptree = Some ptree} a)

    let empty {ic; pc; _} =
      {itree = Option.map I.Tree.empty ic; ptree = Option.map P.Tree.empty pc}

    let is_empty {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.is_empty itree
      | None, Some ptree -> P.Tree.is_empty ptree
      | Some itree, Some ptree ->
          let ib = I.Tree.is_empty itree in
          let pb = P.Tree.is_empty ptree in
          if ib = pb then ib else inconsistent_exit "Tree.is_empty"

    let kind {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.kind itree
      | None, Some ptree -> P.Tree.kind ptree
      | Some itree, Some ptree ->
          let ik = I.Tree.kind itree in
          let pk = P.Tree.kind ptree in
          if ik = pk then ik else inconsistent_exit "Tree.kind"

    let to_value {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.to_value itree
      | None, Some ptree -> P.Tree.to_value ptree
      | Some itree, Some ptree ->
          let open Lwt_syntax in
          let* iv = I.Tree.to_value itree in
          let+ pv = P.Tree.to_value ptree in
          if iv = pv then iv
          else
            inconsistent_exit
              "Tree.to_value %a %a"
              (Misc.Option.pp Misc.Bytes.pp)
              iv
              (Misc.Option.pp Misc.Bytes.pp)
              pv

    let of_value {ic; pc; _} v =
      let open Lwt_syntax in
      match (ic, pc) with
      | None, None -> assert false
      | Some ic, None ->
          let+ itree = I.Tree.of_value ic v in
          {itree = Some itree; ptree = None}
      | None, Some pc ->
          let+ ptree = P.Tree.of_value pc v in
          {ptree = Some ptree; itree = None}
      | Some ic, Some pc ->
          let* itree = I.Tree.of_value ic v in
          let+ ptree = P.Tree.of_value pc v in
          {itree = Some itree; ptree = Some ptree}

    let hash {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, _ ->
          (* If itree exists, we use the hash of it *)
          I.Tree.hash itree
      | None, Some ptree ->
          (* In Plebeia only, we can only have Plebeia hash *)
          P.Tree.hash ptree

    let equal tree1 tree2 =
      let h1 = hash tree1 in
      let h2 = hash tree2 in
      h1 = h2

    let clear ?depth {itree; ptree} =
      Option.iter (I.Tree.clear ?depth) itree ;
      Option.iter (P.Tree.clear ?depth) ptree

    type raw = [`Value of bytes | `Tree of raw String.Map.t]

    let raw_encoding = I.Tree.raw_encoding

    let to_raw {itree; ptree} =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.to_raw itree
      | None, Some ptree -> P.Tree.to_raw ptree
      | Some itree, Some _ptree ->
          (* should do P.Tree.to_raw ptree and compare *)
          I.Tree.to_raw itree

    let of_raw r =
      (* XXX need a mode *)
      {itree = Some (I.Tree.of_raw r); ptree = Some (P.Tree.of_raw r)}

    type repo = I.Tree.repo

    let make_repo = I.Tree.make_repo

    (* XXX? *)
    let unshallow ({itree; ptree} as tree) =
      match itree with
      | Some itree ->
          I.Tree.unshallow itree >|= fun itree -> {itree = Some itree; ptree}
      | None -> Lwt.return tree

    let kinded_key _ = assert false

    let is_shallow _ = assert false

    let config _ = assert false

    let length {itree; ptree} key =
      match (itree, ptree) with
      | None, None -> assert false
      | Some itree, None -> I.Tree.length itree key
      | None, Some ptree -> P.Tree.length ptree key
      | Some itree, Some ptree ->
          I.Tree.length itree key >>= fun ilen ->
          P.Tree.length ptree key >|= fun plen ->
          assert (ilen = plen);
          ilen
  end

  let set_hash_version context hv =
    contexts
      context
      (fun ic -> I.set_hash_version ic hv)
      (fun pc -> P.set_hash_version pc hv)
    @@ function
    | None, None -> assert false
    | Some (Error e), None -> Lwt.return @@ Error e
    | Some (Ok ic), None -> return {context with ic = Some ic}
    | None, Some (Error e) -> Lwt.return @@ Error e
    | None, Some (Ok pc) -> return {context with pc = Some pc}
    | Some (Error e), Some (Error _) -> Lwt.return @@ Error e
    | Some (Error _), Some (Ok _) | Some (Ok _), Some (Error _) ->
        inconsistent_exit "set_hash_version"
    | Some (Ok ic), Some (Ok pc) ->
        return {context with ic = Some ic; pc = Some pc}

  let get_hash_version {ic; pc; _} =
    match (ic, pc) with
    | None, None -> assert false
    | Some ic, None -> I.get_hash_version ic
    | None, Some pc -> P.get_hash_version pc
    | Some ic, Some pc ->
        let ires = I.get_hash_version ic in
        let pres = P.get_hash_version pc in
        if ires <> pres then inconsistent_exit "get_hash_version" ;
        ires

  let sync = do_index I.sync P.sync

  let empty ({ii; pi; _} as i) =
    let ic =
      match (mode, ii) with
      | (`IrminOnly | `Both), Some ii -> Some (I.empty ii)
      | `PlebeiaOnly, _ | (`IrminOnly | `Both), None -> None
    in
    let pc =
      match (mode, pi) with
      | (`PlebeiaOnly | `Both), Some (pi, _) -> Some (P.empty pi)
      | `IrminOnly, _ | (`PlebeiaOnly | `Both), None -> None
    in
    {ic; pc; index = i}

  let retrieve_index oi f =
    match oi with Some i -> f i | None -> lwt_failf "Missing index"

  let commit_genesis ({ii; pi; hacked_patch_context} as index) ~chain_id ~time
      ~protocol =
    (* Hack.
       [hacked_patch_context] modifies [patched_context] in [init].
       This [patched_context] is used by [I.commit_genesis] and [P.commit_genesis]
       through [ii.patch_context] and [pi.patch_context].
    *)
    let ctxt = empty index in
    hacked_patch_context ctxt >>=? fun _ ->
    (* The patched context is delivered via [patched_context] of [init] *)
    match mode with
    | `IrminOnly ->
        retrieve_index ii (fun ii ->
            I.commit_genesis ii ~chain_id ~time ~protocol)
    | `PlebeiaOnly ->
        retrieve_index pi (fun (pi, _) ->
            P.commit_genesis pi ~chain_id ~time ~protocol ~hash_override:None
            >|=? fst)
    | `Both ->
        retrieve_index ii (fun ii ->
            retrieve_index pi (fun (pi, _) ->
                I.commit_genesis ii ~chain_id ~time ~protocol >>=? fun ich ->
                P.commit_genesis
                  pi
                  ~chain_id
                  ~time
                  ~protocol
                  ~hash_override:(Some ich)
                >>=? fun (pch, _) ->
                assert (ich = pch) ;
                return ich))

  let exists {ii; pi; _} ch =
    (match ii with Some ii -> I.exists ii ch | None -> Lwt.return_false)
    >>= function
    | true -> Lwt.return true
    | false -> (
        match pi with
        | Some (pi, iif) ->
            let open Lwt_syntax in
            let* res = P.exists pi ch in
            if res then Lwt.return_true
            else
              (* Ask Icontext *)
              (match ii with
               | Some _ -> Lwt.return_false (* Already asked *)
               | None -> with_temp_icontext iif (fun ii -> I.exists ii ch))
        | None -> Lwt.return_false)

  let checkout =
    let open Lwt_syntax in
    let aux f i v = match i with None -> Lwt.return_none | Some i -> f i v in
    fun ({ii; pi; _} as index) ch ->
      let* co1 = aux I.checkout ii ch in
      let+ co2 =
        (* If not converted yet, convert Irmin store to Plebeia store *)
        match pi with
        | None -> Lwt.return_none
        | Some (pi, iif) -> (
            P.checkout pi ch >>= function
            | Some pc -> Lwt.return_some pc
            | None -> (
                (* Ask Icontext *)
                (* If Icontext exists, use it instead of loading. *)
                (match ii with
                | None -> fun aux -> with_temp_icontext iif aux
                | Some ii -> fun aux -> aux ii)
                @@ fun ii ->
                I.checkout ii ch >>= function
                | None -> Lwt.return_none
                | Some ic ->
                    let {Context.message; time; _} =
                      I.commit_info ic
                    in
                    let* () =
                      Log.lwt_log_notice
                        "checkout: convert Irmin context %a to Plebeia"
                        Tezos_crypto.Context_hash.pp
                        ch
                    in
                    let* pc = P.convert pi ic in
                    (* convert itself does NOT commit the pc.  We commit it here *)
                    let* _ =
                      P.commit ~time ~message pc ~hash_override:(Some ch)
                    in
                    (* pc must be checked out again, so that the parent is set to itself
                       for further modification. *)
                    let+ res = P.checkout pi ch in
                    match res with
                    | None -> assert false
                    | Some pc -> Some pc))
      in
      match (co1, co2) with
      | Some ic, Some pc -> Some {ic = Some ic; pc = Some pc; index}
      | None, None -> None
      | None, Some pc -> Some {ic = None; pc = Some pc; index}
      | Some ic, None -> Some {ic = Some ic; pc = None; index}

  let checkout_exn i ch =
    checkout i ch >>= function
    | None -> Lwt.fail Not_found
    | Some x -> Lwt.return x

  let hash ~time ?message {ic; pc; _} =
    match (ic, pc) with
    | None, None -> assert false
    | Some ic, _ -> I.hash ~time ?message ic
    | None, Some pc ->
        (* In Plebeia only, we can only have Plebeia context hash *)
        P.hash ~time ?message pc

  (* in bytes *)
  let idisk_usage ii =
    let open Lwt_syntax in
    let+ sz = Membench.du (I.path ii) in
    match sz with None -> 0L | Some sz -> Int64.mul sz 1024L

  let pdisk_usage pi = Lwt.return @@ P.get_size pi

  type memory_usage =
    { rss_gb : float
    ; shared_rss_gb : float
    ; non_shared_rss_gb : float
    ; major_collections : int
    ; compactions : int
    }

  let pp_memory_usage ppf { rss_gb; shared_rss_gb; non_shared_rss_gb; major_collections; compactions } =
    Format.fprintf ppf
      "RSS: %.05f GiB; Shared-RSS: %.05f GiB; \
       Non-shared: %.05f GiB; major_collections: %d ; compactions: %d"
      rss_gb
      shared_rss_gb
      non_shared_rss_gb
      major_collections
      compactions

  let memory_usage () =
    let open Lwt_syntax in
    let stat = Gc.quick_stat () in
    let+ smaps = Smaps.get_self_smaps () in
    let smaps = Result.value ~default:[] smaps in
    let rss = Smaps.sum_rss smaps in
    let mmap_rss = Smaps.sum_shared_rss smaps in
    let rss_gb = Stdint.Uint64.to_float rss /. 1024. /. 1024. in
    let shared_rss_gb = Stdint.Uint64.to_float mmap_rss /. 1024. /. 1024. in
    let non_shared_rss_gb = rss_gb -. shared_rss_gb in
    { rss_gb
    ; shared_rss_gb
    ; non_shared_rss_gb
    ; major_collections = stat.major_collections
    ; compactions = stat.compactions
    }

  let print_stat ~level {ii; pi; _} =
    let open Lwt_syntax in
    let* () =
      match ii with
      | Some ii ->
          let* bytes = idisk_usage ii in
          let gb = Int64.to_float bytes /. 1024.0 /. 1024.0 /. 1024.0 in
          Log.lwt_log_notice "level: %ld irmin disk: %.05f GB" level gb
      | _ -> Lwt.return_unit
    in
    let* () =
      match pi with
      | Some (pi, _) ->
          let* bytes = pdisk_usage pi in
          let gb = Int64.to_float bytes /. 1024.0 /. 1024.0 /. 1024.0 in
          Log.lwt_log_notice "level: %ld plebeia disk: %.05f GB" level gb
      | _ -> Lwt.return_unit
    in
    let* musage = memory_usage () in
    Log.lwt_log_notice "level: %ld memory: %a" level pp_memory_usage musage

(*
  (* slow because of reachable_words *)
  let print_pstat_commit_db ~level pi =
    let vc = P.vc pi in
    let cdb = Plebeia.Fs_tree.Vc.commit_db vc in
    Log.lwt_log_notice
      "pcontext: level: %ld commit_db: %d B"
      level
      (Obj.reachable_words (Obj.repr cdb)
*)

  let commit ?level ?expected_context_hash ~time ?message {ic; pc; _} =
    let _level = Option.value level ~default:(-1l) in
    match (ic, pc, expected_context_hash) with
    | None, None, _ -> assert false
    | Some ic, None, _ -> I.commit ~time ?message ic
    | None, Some _, None ->
        P.force_exit
          "Context.commit needs either Icontext or expected_context_hash"
    | None, Some pc, Some expected_context_hash ->
        let open Lwt_syntax in
        let* () =
          Log.lwt_log_notice
            "Committing Plebeia only %a"
            Tezos_crypto.Context_hash.pp
            expected_context_hash
        in
        let+ pch, _plebeia_hash_prefix =
          P.commit ~time ?message pc ~hash_override:(Some expected_context_hash)
        in
        pch
    | Some ic, Some pc, _ ->
        (* We commit Pcontext first.  If Icontext would commit first, then when Pcontext
           commit would fail (due to out of memory), we would have the commit only
           in Icontext.  Checking out such a context would force Pcontext to emulate it
           via Icontext and the missing Pcontext commit would be never created again.
        *)
        let open Lwt_syntax in
        let ich = I.hash ~time ?message ic in
        let* () =
          Log.lwt_log_notice
            "Committing Plebeia and Irmin %a"
            Tezos_crypto.Context_hash.pp
            ich
        in
        let* pch, _plebeia_hash_prefix =
          P.commit ~time ?message pc ~hash_override:(Some ich)
        in
        let+ ich' = I.commit ~time ?message ic in
        assert (ich = ich') ;
        if ich <> pch then inconsistent_exit "commit" ;
        ich

  (* XXX Currently no GC for pcontext *)
  let gc index context_hash =
    do_index (fun ii -> I.gc ii context_hash) (fun _pi -> Lwt.return_unit) index

  (* XXX Currently no GC for pcontext *)
  let wait_gc_completion index =
    do_index (fun ii -> I.wait_gc_completion ii) (fun _pi -> assert false) index

  (* XXX Currently no GC for pcontext *)
  let is_gc_allowed {ii; pi; _} =
    match (ii, pi) with
    | Some ii, Some (_pi, _) -> I.is_gc_allowed ii
    | Some ii, None -> I.is_gc_allowed ii
    | None, Some (_pi, _) -> true
    | None, None -> assert false

  (* XXX We currently do not know what this [split] does *)
  let split {ii; pi; _} =
    match (ii, pi) with
    | Some ii, Some (_pi, _) -> I.split ii
    | Some ii, None -> I.split ii
    | None, Some _ -> ()
    | None, None -> ()

  let set_head index cid ch =
    (* XXX NOP in P.set_head *)
    do_index
      (fun ii -> I.set_head ii cid ch)
      (fun pi -> P.set_head pi cid ch)
      index

  let set_master index ch =
    (* XXX P.set_master fails.  This is never used. *)
    do_index (fun ii -> I.set_master ii ch) (fun pi -> P.set_master pi ch) index

  let get_protocol context =
    do_contexts context I.get_protocol P.get_protocol (fun ph1 ph2 ->
        if ph1 <> ph2 then inconsistent_exit "get_protocol")

  let add_protocol context ph =
    map_contexts
      context
      (fun ic -> I.add_protocol ic ph)
      (fun pc -> P.add_protocol pc ph)

  let get_test_chain context =
    do_contexts context I.get_test_chain P.get_test_chain (fun tcs1 tcs2 ->
        if tcs1 <> tcs2 then inconsistent_exit "get_test_chain")

  let add_test_chain context tcs =
    map_contexts
      context
      (fun ic -> I.add_test_chain ic tcs)
      (fun pc -> P.add_test_chain pc tcs)

  let remove_test_chain context =
    map_contexts context I.remove_test_chain P.remove_test_chain

  let fork_test_chain context ~protocol ~expiration =
    map_contexts
      context
      (fun ic -> I.fork_test_chain ic ~protocol ~expiration)
      (fun pc -> P.fork_test_chain pc ~protocol ~expiration)

  let clear_test_chain index cid =
    do_index
      (fun ii -> I.clear_test_chain ii cid)
      (fun pi -> P.clear_test_chain pi cid)
      index

  let find_predecessor_block_metadata_hash context =
    do_contexts
      context
      I.find_predecessor_block_metadata_hash
      P.find_predecessor_block_metadata_hash
      (fun bmho1 bmho2 ->
        if bmho1 <> bmho2 then
          inconsistent_exit "get_predecessor_block_metadata_hash")

  let add_predecessor_block_metadata_hash context bmh =
    map_contexts
      context
      (fun ic -> I.add_predecessor_block_metadata_hash ic bmh)
      (fun pc -> P.add_predecessor_block_metadata_hash pc bmh)

  let find_predecessor_ops_metadata_hash context =
    do_contexts
      context
      I.find_predecessor_ops_metadata_hash
      P.find_predecessor_ops_metadata_hash
      (fun omssho1 omssho2 ->
        if omssho1 <> omssho2 then
          inconsistent_exit "find_predecessor_ops_metadata_hash")

  let add_predecessor_ops_metadata_hash context omssh =
    map_contexts
      context
      (fun ic -> I.add_predecessor_ops_metadata_hash ic omssh)
      (fun pc -> P.add_predecessor_ops_metadata_hash pc omssh)

  let merkle_tree {ic; pc; _} leaf_kind key =
    match (ic, pc) with
    | Some ic, _ -> I.merkle_tree ic leaf_kind key
    | None, _ -> lwt_failf "merkle_tree: required Icontext"

  let merkle_tree_v2 _ctx _leaf_kind _key = assert false

  module Checks = I.Checks

  let restore_context {ii; _} ~expected_context_hash ~nb_context_elements ~fd
      ~in_memory ~progress_display_mode =
    match ii with
    | Some ii ->
        I.restore_context
          ii
          ~expected_context_hash
          ~nb_context_elements
          ~fd
          ~in_memory
          ~progress_display_mode
    | None -> lwt_failf "restore_context: missing Irmin"

  let dump_context {ii; _} data ~fd ~on_disk ~progress_display_mode =
    match ii with
    | Some ii -> I.dump_context ii data ~fd ~on_disk ~progress_display_mode
    | None -> lwt_failf "dump_context: missing Irmin"

  (* include Tezos_context_helpers.Context.Make_config (Conf) *)
  let equal_config _ = assert false

  let config _ = assert false

  (* include Tezos_context_helpers.Context.Make_proof (Store) (Conf) *)
  module Proof = struct
    include Tezos_context_sigs.Context.Proof_types
  end

  let produce_tree_proof _ = assert false

  let verify_tree_proof _ = assert false

  let produce_stream_proof _ = assert false

  let verify_stream_proof _ = assert false

  type node_key = I.node_key

  type value_key = I.value_key

  type kinded_key = I.kinded_key

  let length _ = assert false

  let to_memory_tree _ = assert false

  let is_empty _ = assert false

  let flush _ = assert false

  let underlying_contexts {ic; pc; _} = (ic, pc)
end
