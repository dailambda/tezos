Setting up the node
===================

A node in the Tezos network provides different configuration possibilities:

- tune various parameters of the node using flexible combinations of: a configuration file, command-line options, and environment variables
- specify the Tezos network to connect to, which can be the Mainnet or different test networks.
- configure the amount of history kept by the node according to different tradeoffs, using history modes
- rapidly catch up with a given (main or test) network by loading network snapshots

These possibilities are described in the following pages.

.. toctree::
   :maxdepth: 2

   node-configuration

.. toctree::
   :maxdepth: 2

   multinetwork

.. toctree::
   :maxdepth: 2

   history_modes

.. toctree::
   :maxdepth: 2

   snapshots

.. toctree::
   :maxdepth: 2

   node-monitoring
