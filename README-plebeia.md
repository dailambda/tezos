# Tezos + Plebeia

## Install

```
$ make build-dev-deps
$ ./0build.sh  # Install Plebeia
$ make
```

## Modes

By changing `mode` of `src/lib_context/xcontext.ml`, the node runs in 3 modes:

```
let mode = `Both (* or `IrminOnly , `PlebeiaOnly *)
```

```Both``
:   Runs the both backend.  The behaviour of Plebeia back end is compared with Irmin.

```IrminOnly`
:   Runs Irmin backend only.  Should be as same as the vanilla node.

```PlebeiaOnly`
:   Runs Plebeia backend only.  Context hashes are cheated from the block header.

## Plebeia integratation

For the simplicity for the integration, the path encoding optimization is completely disabled for now: it uses the trivial 8bit/1char encoding.  For example, a script expr hash whose text representation is 54 chars of `[0-9a-f]` is simply translated to 432 bit segments.  Plebeia has huge overhead for such long segments.

## Benchmark

Replay+reconstructing Tezos mainnet blocks from 1466301 to 1481000.

### Plebeia only

* Context conversion : 9m49s
* 1466301..1481000: 58m44s

### Irmin only

* No context conversion
* 1466301..148100: 52m44s

### Both

* Context conversion :  9m55s
* 1466301..148100: 1h29m14s 

Analysis:

* Core: 110m - 89m = 21m
* Plebeia: 37m44s
* Irmin: 31m44s

### Variants

#### Plebeia only, 6bits/char

* Context conversion : 8m53s
* 1466301..148100: 54m50s

#### Plebeia only, names compressed

Names converted to 16bit hashes

* Context conversion 8m51s
* 1466301..148100: 49m33s

#### Plebeia only, names compressed, Fs debug removed

Names converted to 16bit hashes

* Context conversion 8m47s
* 1466301..148100: 49m14s

Almost negligible...

#### Plebeia only, name compressed, Fs2

* Context conversion 7m02s
* 1466301..148100: 46m55s

#### Plebeia only, name compressed, Fs2 with some optimizations: 

```
commit f0d88b90ccbcfc10fe39c4d84a1a3984cf0f4303

    27 byte hash for simplicity

commit d8d0accdf3d021776e3b9f257027508680a41fde

    Stopped to load immediately the previous cell of Internal

commit df722c0069de5e707c4c44f9030339556cfb579a

    stopped always loading the sub node under Bud
```

* conversion: 7m06s
* 1466301..148100: 46m08s

#### Plebeia only, name compressed, Fs2 with some optimizations, lazy hash loading

* conversion 7m15s
* 1466301..148100: 47m17s

## Env vars

### `PCONTEXT_NAME`

Path name encoding

* `8bits`: `Name8bits`
* `6bits`: `Name6bits`
* otherwise, `NameCompressed` is used

### `PCONTEXT_DISABLE_CURSOR_CACHE`

* non empty: disable the cursor cache
* empty: enable the cursor cache

### `CONTEXT_SUBSYSTEM`

* `Both` : Run the both context subsystems
* `PlebeiaOnly` : Plebeia only
* `IrminOnly` : Irmin only
